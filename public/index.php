<?php
$t=microtime(true);
session_start();
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require('..'.DS.'core'.DS.'definitions.php');
require(CORE_DIR.DS.'initializer.php');
if($_SERVER['HTTPS'] !== 'on') header('Location: https://'.$_SERVER["HTTP_HOST"].'/'.trim(parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH), '/\\'));
echo '<!-- Page loaded in ',microtime(true)-$t,'s -->';
