<?php
/**
 * @author Mateus Demboski <mateus@hospedasul.com>
 * @since 04/04/14
 */
//verify if user is logged in before start the system requests.
if(!isset($_SESSION['user']['id']) or empty($_SESSION['user']['id'])){ header('Location: /login.php?notLogged'); exit; }

//Initialize system requests.
require(CORE_DIR . DS . 'config.php');
require(CORE_DIR . DS . 'functions.php');
if(file_exists(VENDOR_DIR . DS . 'autoload.php')) require(VENDOR_DIR . DS . 'autoload.php');
else die('You have already installed the dependencies with composer?');

//Set the connection to Database
$dbconfig = new \Doctrine\DBAL\Configuration();

$database = \Doctrine\DBAL\DriverManager::getConnection($configurations['database'],$dbconfig);

//Set configurations for Smarty Template Engine
$smarty = new Smarty();
$smarty->setTemplateDir(DIRECTORY_ROOT . DS . 'views' . DS);
$smarty->setCompileDir(DIRECTORY_ROOT . DS . 'tmp' . DS . 'templates_c' . DS);
$smarty->setConfigDir(CORE_DIR . DS . 'smarty_configs' . DS);
$smarty->setCacheDir(DIRECTORY_ROOT . DS . 'tmp' . DS . 'cache' . DS);

//Set routing, template/system default variables and params
$route = new Routes('pages', trim( parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH ), '/\\' ) );
$route->run();

$path = $route->getPathForURL();
$smarty->assign('site', $configurations['SITE_URL']);
$smarty->assign('path', $path);

$pageFile = DIRECTORY_ROOT . DS . $route->getPathForInclude();

$params = (array) $route->getParamsFromURL();
//rename the original variable to a more friendly
$action = !empty($params[0]) ? $params[0] : '';

//prevent the session hijacking
if(session_status() == PHP_SESSION_ACTIVE && !isset($_SESSION['PREVENT_SESSION_HIJACKING']))
    $_SESSION['PREVENT_SESSION_HIJACKING'] = md5( $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'] );
elseif(session_status() != PHP_SESSION_ACTIVE || !isset($_SESSION['PREVENT_SESSION_HIJACKING']) || $_SESSION['PREVENT_SESSION_HIJACKING'] != md5( $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR']) ){
    exit('DETECTAMOS UM POSSIVEL ROUBO DE SESS&Atilde;O, POR FAVOR, <strong>FECHE SEU NAVEGADOR</strong> E INICIE UMA NOVA SESS&Atilde;O EM NOSSO PAINEL!');
}

//verify and include the requested page
if(file_exists($pageFile)){
    //check if user has permission
    if(hasPermission($database,$_SESSION['user']['id_role'],$route->getFileName(), $action)){
        include(CORE_DIR . DS . 'load.ini.php');
        include($pageFile);
        $smarty->assign('route',  $route->getFileName());
        $smarty->assign('action', $action);
        $smarty->assign('params', $params);

        if(isset($displayTpl)) $smarty->assign('mid','../views/' . $displayTpl . '.tpl');
        else $smarty->assign('mid','../views/' . $route->getFileName() . '.tpl');
    }else{
        //if user haven't permission, the system return a error page and network response 403 (Forbidden)
        header("HTTP/1.0 403 Forbidden");
        $smarty->assign('mid', '../views/default/403.tpl');
        $smarty->assign('pageTitle','Erro 403 - Forbidden!');
    }
}else{
    //action if page not fund.
    header("HTTP/1.0 404 Not Found");
    $smarty->assign('mid', '../views/default/404.tpl');
    $smarty->assign('pageTitle','Erro!');
    $smarty->assign('mensage', ['type'=>'danger', 'dismissible'=>true, 'value'=>"ROUTE '{$route->getFileName()}' NOT FOUND! (error 404)"]);
}

$smarty->display('layout.tpl');