<?php
$queryContactsStared_Inbox = $database->createQueryBuilder();
$queryContactsStared_Inbox -> select('id, name, subject, date')
                           -> from('contacts')
                           -> where('box=:boxName AND stared="Y"')
                           -> setParameter(':boxName','Inbox');
$contactsStared_InboxStatement = $queryContactsStared_Inbox->execute();
$contactsStared_Inbox = $contactsStared_InboxStatement->fetchAll(\PDO::FETCH_ASSOC);
//parse data to template engine
$smarty->assign('contactsStared_Inbox',$contactsStared_Inbox);

$queryMenuPages = $database->createQueryBuilder();
$queryMenuPages -> select('name, slug') -> from('pages');
$menuPagesStatement = $queryMenuPages->execute();
$menuPages = $menuPagesStatement->fetchAll(\PDO::FETCH_ASSOC);
//parse data to template engine
$smarty->assign('menuPages',$menuPages);