<?php
$configurations = [
    'database' => [
        'driver'   => 'pdo_mysql',
        'host'     => 'localhost',
        'user'     => 'testes',
        'port'     => 3306,
        'password' => 'abc@123',
        'dbname'   => 'panel',
        'charset'  => 'utf8',
    ],
    'password' => [
        'cost' => 8,
        'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
    ],
    'SITE_URL'         => 'https://wernercalcadosonline.dev',
    'UPLOAD_DIRECTORY' => '/home/servidor/public_www/wernercalcadosonline/public/images/content',
];
