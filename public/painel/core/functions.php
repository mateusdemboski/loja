<?php
/**
 * @see http://php.net/mcrypt#107483
 * @param string $decrypted
 * @param string $password
 * @param string $salt
 * @return bool|string
 */
function encrypt($decrypted, $password, $salt='!kQm*fF3pXe1Kbm%9') {
    // Build a 256-bit $key which is a SHA256 hash of $salt and $password.
    $key = hash('SHA256', $salt . $password, true);
    // Build $iv and $iv_base64.  We use a block size of 128 bits (AES compliant) and CBC mode.  (Note: ECB mode is inadequate as IV is not used.)
    srand(); $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
    if (strlen($iv_base64 = rtrim(base64_encode($iv), '=')) != 22) return false;
    // Encrypt $decrypted and an MD5 of $decrypted using $key.  MD5 is fine to use here because it's just to verify successful decryption.
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $decrypted . md5($decrypted), MCRYPT_MODE_CBC, $iv));
    // We're done!
    return $iv_base64 . $encrypted;
}

/**
 * @see http://php.net/mcrypt#107483
 * @param string $encrypted
 * @param string $password
 * @param string $salt
 * @return bool|string
 */
function decrypt($encrypted, $password, $salt='!kQm*fF3pXe1Kbm%9') {
    // Build a 256-bit $key which is a SHA256 hash of $salt and $password.
    $key = hash('SHA256', $salt . $password, true);
    // Retrieve $iv which is the first 22 characters plus ==, base64_decoded.
    $iv = base64_decode(substr($encrypted, 0, 22) . '==');
    // Remove $iv from $encrypted.
    $encrypted = substr($encrypted, 22);
    // Decrypt the data.  rtrim won't corrupt the data because the last 32 characters are the md5 hash; thus any \0 character has to be padding.
    $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($encrypted), MCRYPT_MODE_CBC, $iv), "\0\4");
    // Retrieve $hash which is the last 32 characters of $decrypted.
    $hash = substr($decrypted, -32);
    // Remove the last 32 characters from $decrypted.
    $decrypted = substr($decrypted, 0, -32);
    // Integrity check.  If this fails, either the data is corrupted, or the password/salt was incorrect.
    if (md5($decrypted) != $hash) return false;
    // Yay!
    return $decrypted;
}

/**
 * @see http://stackoverflow.com/a/14114419/3352122
 * @param $string
 * @return mixed
 */
function clean($string) {
    $string = strtolower($string);
    $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

/**
 * HTTP Function to check if request method is POST
 * this function is unnecessary, but leaves the most beautiful code... = P
 *
 * @return bool
 */
function isPost(){
    return $_SERVER['REQUEST_METHOD'] === 'POST';
}

/**
 * Function to check if user role have permission to access a page/action.
 *
 * @param PDO $connection
 * @param int $idRole
 * @param string $page
 * @param string null $action
 * @return bool
 */
function hasPermission(\Doctrine\DBAL\Connection $connection, $idRole, $page = 'home', $action = null){
    $queryRolePermissions = $connection->createQueryBuilder();
    $queryRolePermissions -> select('p.url')
                          -> from('permissions','p')
                          -> innerJoin('p', 'role_permissions', 'rp', 'p.id = rp.id_permission')
                          -> where('rp.id_role = :idRole')
                          -> setParameter(':idRole',$idRole, PDO::PARAM_INT);
    $rolePermissionsStatement = $queryRolePermissions->execute();
    $rolePermissions = $rolePermissionsStatement -> fetchAll(\PDO::FETCH_ASSOC);
    $urlToMatch = $page . '/' . $action;
    $response = false;
    foreach($rolePermissions as $permission)
        if($urlToMatch == $permission['url'] || $permission['url'] == $page.'/*'){ $response = true; break;}

    return $response;
}

/**
 * @param $base
 * @param $imageSizeY
 * @param $imageSizeX
 * @return array
 */
function calc_position($base, $imageSizeX, $imageSizeY){
    //Calcula a posição da imagem em relação a base
    if($base['height'] == $imageSizeY && $base['width'] != $imageSizeX) {
        $positionY = 0;
        $calcPositionX = round(($base['width'] - $imageSizeX) / 2);
        $positionX = ($calcPositionX < 0) ? 0 : $calcPositionX;
    }elseif($base['width'] == $imageSizeX && $base['height'] != $imageSizeY){
        $positionX = 0;
        $calcPositionY = round(($base['height']-$imageSizeY)/2);
        $positionY = ($calcPositionY < 0) ? 0 : $calcPositionY;
    }elseif($base['width'] > $imageSizeX && $base['height'] > $imageSizeY){
        $calcPositionX = round(($base['width'] - $imageSizeX) / 2);
        $positionX = ($calcPositionX < 0) ? 0 : $calcPositionX;
        $calcPositionY = round(($base['height']-$imageSizeY)/2);
        $positionY = ($calcPositionY < 0) ? 0 : $calcPositionY;
    }
    else{$positionX=0;$positionY=0;}

    return array('x'=>$positionX, 'y'=>$positionY);
}

/**
 * @return string
 */
function rand_passwd(){
    $passwd = base64_encode( MD5( uniqid( rand(), true ) ) );
    $passwd_size = strlen($passwd);
    $start = rand(0,$passwd_size);
    $length  = rand($start,$passwd_size);

    return substr($passwd, $start, $length);
}