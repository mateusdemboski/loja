<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {$message.value}
            </div>
        {/if}
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Banners do Site</h3>
                <a href="{$path}/banners/adicionar/{$params.1}" class="btn btn-danger pull-right">Adicionar novo</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Imagem</th>
                        <th>Nome</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$images item=image}<tr>
                        <td>{$image.id}</td>
                        <td class="text-center"><img style="width:100px;" src="{$site}/img/banner/{$image.name}"></td>
                        <td>{$image.title}</td>
                        <td class="text-center"><i class="fa fa-{if $image.active=='N'}times{else}check{/if}-circle"></i><span class="sr-only">{if $image.active=='Y'}1{else}0{/if}</span></td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs" data-toggle="tooltip" title="Editar" href="{$path}/banners/editar/{$image.id_reference}/{$image.id}"><i class="fa fa-pencil-square-o"></i></a>
                            <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="Remover" onclick="confirm('Você tem certeza que deseja remover isso?')" href="{$path}/banners/remover/{$image.id_reference}/{$image.id}"><i class="fa fa-times"></i></a>
                        </td>
                        </tr>{/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Imagem</th>
                        <th>Nome</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->