<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger" enctype="multipart/form-data">
            <div class="box-header with-border">
                <h3 class="box-title">Imagens de produto <small>Adicionar Novo</small></h3>
                <a href="{$path}/produto-imagens/listar/{$params.1}" class="btn btn-default pull-right" style="margin-right:5px"><i class="fa fa-fw fa-chevron-left"></i> Voltar</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="photo">Foto</label>
                    <input type="file" class="form-control" name="photo" id="photo"/>
                    <input type="hidden" name="photoOriginal" value="{$name}">
                    <p class="help-block">Procure utilizar imagens com largura e altura no tamnho de 560px X 840px ou proporcionais.</p>
                    {if $action == 'editar'}<p class="help-block">Deixe em branco caso não queira alterar a imagem.</p>{/if}
                </div>
                <div class="form-group">
                    <label for="title">Titulo</label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="Titulo" maxlength="255" value="{$title}" required=""/>
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <input type="text" class="form-control" name="description" id="description" placeholder="Descrição" maxlength="175" value="{$description}" required=""/>
                </div>
                <div class="form-group">
                    <label class="control-label">Imagem ativo?&nbsp;</label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="Y"{if $active != 'N'} checked=""{/if}> Sim
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="N"{if $active == 'N'} checked=""{/if}> Não
                    </label>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <input type="hidden" name="id_reference" value="{$params.1}">
                <input type="hidden" name="id" value="{$params.2}">
                <input type="hidden" name="action" value="addOrEditProductImage">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->