{if isset($message)}
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        </div>
    </div>
{/if}
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Cores de produto <small>{$productName}</small></h3>
                <a href="{$path}/cores/produto/{$params.1}/associar" class="btn btn-danger pull-right">Associar/desassociar cores ao produto</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Cor</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$colorsByProduct item=color}
                    <tr>
                        <td>{$color.id}</td>
                        <td>{$color.name}</td>
                        <td style="background-color:{$color.hex};"><div><span class="sr-only">{$color.hex}</span></div></td>
                        <td class="text-center"><i class="fa fa-{if $color.active=='N'}times{else}check{/if}-circle"></i></td>
                        <td class="text-center">
                            <a href="{$path}/tamanhos/porcordeproduto/{$color.id_product_color}" class="btn btn-default btn-xs">&nbsp;<i class="fa fa-arrows-v"></i>&nbsp;</a>
                        </td>
                    </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Cor</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->