<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Cores de Produto <small>Associar cores</small></h3>
                <a href="{$path}/cores/produto/{$params.1}" class="btn btn-default pull-right"><i class="fa fa-fw fa-chevron-left"></i> Voltar</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <button type="button" class="checkbox-toggle btn btn-danger">Marcar/Desmarcar todas as cores</button><br><br>
                {foreach from=$colors item=color}
                <div class="form-group">
                    <label>
                        <input type="checkbox" name="colors[]" value="{$color.id}" {if $color.id|in_array:$product_colors}checked=""{/if}> <div style="display:inline-block;width:20px;height:15px;background:{$color.hex}"></div> {$color.name}
                    </label>
                </div>
                {/foreach}
            </div><!-- /.box-body -->
            <div class="box-footer">
                <input type="hidden" name="id" value="{$params.1}">
                <input type="hidden" name="action" value="updateProductColors">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button onclick="location.reload()" class="btn btn-danger"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->