<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger" enctype="multipart/form-data">
            <div class="box-header with-border">
                <h3 class="box-title">Cores <small>Adicionar Nova</small></h3>
            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nome" maxlength="255" value="{$color.name}" onkeyup="makeSlug(this.value)" required=""/>
                </div><!-- /.form group -->

                <!-- Color Picker -->
                <div class="form-group">
                    <label for="hex">Cor:</label>
                    <div class="input-group color-picker">
                        <div class="input-group-addon">
                            <i></i>
                        </div>
                        <input id="hex" name="hex" value="{$color.hex}" type="text" class="form-control"/>
                    </div><!-- /.input group -->
                </div><!-- /.form group -->

                </div>

                <div class="form-group">
                    <label class="control-label">Cor de produto ativo?&nbsp;</label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="Y"{if $color.active != 'N'} checked=""{/if}> Sim
                    </label><!-- /.radio inline -->
                    <label class="radio-inline">
                        <input type="radio" name="active" value="N"{if $coloractive == 'N'} checked=""{/if}> Não
                    </label><!-- /.radio inline -->
                </div><!-- /.form group -->

            </div><!-- /.box-body -->

            <div class="box-footer">
                <input type="hidden" name="id" value="{$params.1}">
                <input type="hidden" name="slug" id="slug" value="{$color.slug}">
                <input type="hidden" name="action" value="addOrEditColor">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">


    function slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáâẽéêíõóôúñç·/_,:;";
        var to   = "aaaaeeeiooounc------";
        for (var i=0, l=from.length ; i<l ; i++)
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

        return str;
    }
    function makeSlug(text){
        document.getElementById('slug').value = slug(text);
    }
</script>