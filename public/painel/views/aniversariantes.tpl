<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.value}
            </div>
        {/if}
        <form method="post" class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Desconto para aniversáriantes</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="container-fluid">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="name">Valor do desconto</label>
                            <input class="form-control" name="name" id="name" placeholder="Valor do desconto" type="number"/>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="type">Tipo do desconto</label>
                            <select class="form-control" name="type" id="type">
                                <option value="P">Porcentagem (%)</option>
                                <option value="V">Valor (R$)</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Ativar desconto?&nbsp;</label>
                            <div class="form-group">
                                <label class="radio-inline">
                                    <input type="radio" name="active" value="Y"{if $page.active == 'Y'} checked=""{/if}> Sim
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="active" value="N"{if $page.active == 'N'} checked=""{/if}> Não
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Atualizar</button>
                <button onclick="location.reload()" class="btn btn-danger"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->