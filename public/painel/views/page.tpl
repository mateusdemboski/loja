<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.value}
            </div>
        {/if}
        <form method="post" class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Edição de página</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Titulo</label>
                    <input class="form-control" name="name" id="name" placeholder="Titulo" value="{$page.name}" maxlength="50"/>
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <input class="form-control" name="description" id="description" placeholder="Descrição" value="{$page.description}" maxlength="155"/>
                </div>
                <div class="form-group">
                    <label for="compose-textarea">Texto</label>
                    <textarea id="compose-textarea" name="text" class="form-control" style="height: 300px">
                      {$page.text}
                    </textarea>
                </div>
                <div class="form-group">
                    <label class="control-label">Página ativa:&nbsp;</label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="Y"{if $page.active == 'Y'} checked=""{/if}> Sim
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="N"{if $page.active == 'N'} checked=""{/if}> Não
                    </label>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button onclick="location.reload()" class="btn btn-danger"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->