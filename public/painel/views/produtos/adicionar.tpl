<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger" enctype="multipart/form-data">
            <div class="box-header with-border">
                <h3 class="box-title">Produtos <small>Adicionar Novo</small></h3>
            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    <label for="photo">Foto Principal</label>
                    <input type="file" class="form-control" name="photo" id="photo" accept="image/jpeg,image/png,image/gif" {if $action != 'editar'}required=""{/if}/>
                    <input type="hidden" name="photoOriginal" value="{$product.image}">
                    {if $action == 'editar'}<p class="help-block">Deixe em branco caso não queira alterar a foto deste produto.</p>{/if}
                </div><!-- /.form group -->

                <div class="form-group">
                    <label for="reference">Referência</label>
                    <input type="number" class="form-control" name="reference" id="reference" placeholder="Referência" maxlength="10" value="{$product.reference}" required=""/>
                </div><!-- /.form group -->

                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nome" maxlength="255" value="{$product.name}" onkeyup="makeSlug(this.value)" required=""/>
                </div><!-- /.form group -->

                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" class="form-control" name="slug" id="slug" placeholder="slug" maxlength="255" value="{$product.slug}" required=""/>
                </div><!-- /.form group -->

                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control ckeditor" name="description" id="description" rows="6" placeholder="Descrição" required="">{$product.description}</textarea>
                </div><!-- /.form group -->

                <div class="form-group">
                    <label for="specification">Especificação</label>
                    <textarea class="form-control ckeditor" name="specification" id="specification" rows="6" placeholder="Especificação" required="">{$product.specification}</textarea>
                </div><!-- /.form group -->

                <div class="row">
                    <div class="form-group col-sm-4">
                        <label for="cost_price">Preço de custo:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                            </div>
                            <input type="text" id="cost_price" name="cost_price" class="form-control mask-money" value="{$product.cost_price|number_format:2:',':'.'}" required=""/>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-4">
                        <label for="price">Preço:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                            </div>
                            <input type="text" id="price" name="price" class="form-control mask-money" value="{$product.price|number_format:2:',':'.'}" required=""/>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-4">
                        <label for="promotional_price">Preço Promocional:</label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-money"></i>
                            </div>
                            <input type="text" id="promotional_price" name="promotional_price" value="{$product.promotional_price|number_format:2:',':'.'}" class="form-control mask-money"/>
                        </div><!-- /.input group -->
                    </div><!-- /.form group -->
                </div><!-- /.row -->

                <div class="row">

                    <div class="form-group col-sm-2">
                        <label for="parcel_max">Parcelas</label>
                        <input type="number" class="form-control" name="parcel_max" id="parcel_max" placeholder="Parcelas" min="0" max="99" value="{$product.parcel_max}" required=""/>
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-2">
                        <label for="stock">Estoque</label>
                        <input type="number" class="form-control" name="stock" id="stock" placeholder="Estoque" min="0" max="9999" value="{$product.stock}" required=""/>
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-3">
                        <label for="weight">Peso</label>
                        <input type="text" class="form-control weight" name="weight" id="weight" placeholder="Peso" value="{$product.weight|number_format:3:',':''}"/>
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-3">
                        <label for="department">Departamento</label>
                        <select class="form-control" name="department" id="department" required="">
                            {foreach from=$departments item=department}
                                <option value="{$department.id}" {if $product.id_department == $department.id}select=""{/if}>{$department.name}</option>
                            {/foreach}
                        </select>
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-2">
                        <label for="deadline">Prazo de entrega</label>
                        <input type="number" class="form-control" name="deadline" id="deadline" placeholder="Prazo de entrega" value="{$product.deadline}"/>
                    </div><!-- /.form group -->

                </div>

                <div class="row">

                    <div class="form-group col-sm-4">

                        <label class="control-label">Frete Grátis?&nbsp;</label>
                        <label class="radio-inline">
                            <input type="radio" name="free_shipping" value="Y"{if $product.free_shipping == 'S'} checked=""{/if}> Sim
                        </label><!-- /.radio inline -->
                        <label class="radio-inline">
                            <input type="radio" name="free_shipping" value="N"{if $product.free_shipping != 'S'} checked=""{/if}> Não
                        </label><!-- /.radio inline -->
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-4">
                        <label class="control-label">Destaque na home?&nbsp;</label>
                        <label class="radio-inline">
                            <input type="radio" name="featured_in_home" value="Y"{if $product.featured_in_home != 'N'} checked=""{/if}> Sim
                        </label><!-- /.radio inline -->
                        <label class="radio-inline">
                            <input type="radio" name="featured_in_home" value="N"{if $product.featured_in_home == 'N'} checked=""{/if}> Não
                        </label><!-- /.radio inline -->
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-4">
                        <label class="control-label">Destaque no departamento?&nbsp;</label>
                        <label class="radio-inline">
                            <input type="radio" name="featured_in_department" value="Y"{if $product.featured_in_department != 'N'} checked=""{/if}> Sim
                        </label><!-- /.radio inline -->
                        <label class="radio-inline">
                            <input type="radio" name="featured_in_department" value="N"{if $product.featured_in_department == 'N'} checked=""{/if}> Não
                        </label><!-- /.radio inline -->
                    </div><!-- /.form group -->

                    <div class="form-group col-sm-4">
                        <label class="control-label">Produto ativo?&nbsp;</label>
                        <label class="radio-inline">
                            <input type="radio" name="active" value="Y"{if $product.active != 'N'} checked=""{/if}> Sim
                        </label><!-- /.radio inline -->
                        <label class="radio-inline">
                            <input type="radio" name="active" value="N"{if $product.active == 'N'} checked=""{/if}> Não
                        </label><!-- /.radio inline -->
                    </div><!-- /.form group -->

                </div><!-- /.row -->

            </div><!-- /.box-body -->

            <div class="box-footer">
                <input type="hidden" name="id" value="{$params.1}">
                <input type="hidden" name="action" value="addOrEditProduct">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">


    function slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "ãàáâẽéêíõóôúñç·/_,:;";
        var to   = "aaaaeeeiooounc------";
        for (var i=0, l=from.length ; i<l ; i++)
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                .replace(/\s+/g, '-') // collapse whitespace and replace by -
                .replace(/-+/g, '-'); // collapse dashes

        return str;
    }
    function makeSlug(text){
        document.getElementById('slug').value = slug(text);
    }
</script>