<div class="box">
    <div class="box-header">
        <h3 class="box-title"></h3>
    </div>
    <div class="box-body">
        <p><strong>Você deseja realmente sair do painel de controle?</strong></p>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-xs-6">
                <a href="{$path}/" class="btn btn-default pull-right">Não, voltar para a home</a>
            </div>
            <div class="col-xs-6">
                <form method="post">
                    <button type="submit" name="confirm" class="btn btn-warning">Sim, sair do painel</button>
                </form>
            </div>
        </div>
    </div>
</div>