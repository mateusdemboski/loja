{if isset($message)}
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
            {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
            {$message.text}
        </div>
    </div>
</div>
{/if}
<div class="row">
    <div class="col-md-3">
        <a href="{$path}/contatos/compor" class="btn btn-danger btn-block margin-bottom">Compor</a>
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Pastas</h3>
            </div>
            <div class="box-body no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <li {if $smarty.session.contact_box=='Inbox'}class="active"{/if}><a href="{$path}/contatos/open/Inbox"><i class="fa fa-inbox"></i> Caixa de entrada <span class="label label-primary pull-right">{$contactsStared_Inbox|count}</span></a></li>
                    <li {if $smarty.session.contact_box=='Sent'}class="active"{/if}><a href="{$path}/contatos/open/Sent"><i class="fa fa-envelope-o"></i> Itens Enviados</a></li>
                    <li {if $smarty.session.contact_box=='Outbox'}class="active"{/if}><a href="{$path}/contatos/open/Outbox"><i class="fa fa-sign-out"></i> Caixa de Saída</a></li>
                    <li {if $smarty.session.contact_box=='Trash'}class="active"{/if}><a href="{$path}/contatos/open/Trash"><i class="fa fa-trash-o"></i> Lixeira</a></li>
                </ul>
            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div><!-- /.col -->
    <div class="col-md-9">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {if $smarty.session.contact_box == 'Sent'}Itens Enviados
                    {elseif $smarty.session.contact_box == 'Outbox'}Caixa de Saída
                    {elseif $smarty.session.contact_box == 'Trash'}Lixeira
                    {else}Caixa de entrada{/if}
                </h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive mailbox-messages" style="overflow:hidden;">
                    <table id="dataTable" class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><i class="fa fa-star text-yellow"></i></th>
                            <th>Nome</th>
                            <th>Assunto</th>
                            <th>Anexo</th>
                            <th>Data</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach from=$contacts item=contact}
                        <tr id="message{$contact.id}">
                            <td><input type="radio" name="id_contact" value="{$contact.id}" /></td>
                            <td class="mailbox-star"><a href="#"><i class="fa fa-star{if $contact.stared == 'N'}-o{/if} text-yellow"></i></a></td>
                            <td class="mailbox-name"><a href="{$path}/contatos/ler/{$contact.id}/{$contact.subject|lower|replace:' ':'-'|regex_replace:'/[^A-Za-z0-9\-]/':''|regex_replace:'/-+/':'-'}/">{$contact.name}</a></td>
                            <td class="mailbox-subject"><b>{$contact.subject}</b></td>
                            <td class="mailbox-attachment"></td>
                            <td class="mailbox-date">{$contact.date|date_format:'d/m/y'}</td>
                        </tr>
                        {/foreach}
                        </tbody>
                    </table><!-- /.table -->
                </div><!-- /.mail-box-messages -->
            </div><!-- /.box-body -->
            <div class="box-footer no-padding">
                <div class="mailbox-controls">
                    <!-- Check all button -->
                    {*<button class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>*}
                    <div class="btn-group">
                        <button id="excludeMessage" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                        <button id="replyMessage" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                        <button id="forwardMessage" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                    </div><!-- /.btn-group -->
                    <button onclick="window.location.reload();" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </div><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->