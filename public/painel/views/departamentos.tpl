<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Hover Data Table</h3>
                <a href="{$path}/departamentos/adicionar" class="btn btn-danger pull-right">Adicionar novo</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$departments item=department}
                        <tr>
                            <td>{$department.id}</td>
                            <td>{$department.name}</td>
                            <td>{$department.slug}</td>
                            <td class="text-center"><i class="fa fa-{if $department.active=='N'}times{else}check{/if}-circle"></i> </td>
                            <td class="text-center">
                                <a href="{$path}/departamentos/editar/{$department.id}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                <a class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Slug</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->