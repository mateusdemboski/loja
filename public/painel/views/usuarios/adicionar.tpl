<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger" enctype="multipart/form-data">
            <div class="box-header with-border">
                <h3 class="box-title">Usuário do sistema <small>Adicionar Novo</small></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="photo">Foto</label>
                    <input type="file" class="form-control" name="photo" id="photo"/>
                    <input type="hidden" name="photoOriginal" value="{$image}">
                    <p class="help-block">Para evitar distorções, utilize imagens com largura e altura iguais. Ex.: 150px X 150px.</p>
                    {if $action == 'editar'}<p class="help-block">Deixe em branco caso não queira alterar a foto deste usuário.</p>{/if}
                </div>
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nome" maxlength="255" value="{$name}" required=""/>
                </div>
                <div class="form-group">
                    <label for="username">Usuário</label>
                    <input type="text" class="form-control" name="username" id="username" placeholder="Usuário" maxlength="50" value="{$username}" required=""/>
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" maxlength="255" value="{$email}" required=""/>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="password">Senha</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Senha" maxlength="20" {if $action == 'adicionar'}required=""{/if}/>
                            {if $action == 'editar'}<p class="help-block">Deixe em branco caso não queira alterar a senha deste usuário.</p>{/if}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="role">Função Administrativa</label>
                            <select class="form-control" name="role" id="role">
                                {foreach from=$roles item=role}<option value="{$role.id}" {if $role.id == $id_role}selected="" {/if}>{$role.name}</option>{/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Usuário ativo?&nbsp;</label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="Y"{if $active != 'N'} checked=""{/if}> Sim
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="N"{if $active == 'N'} checked=""{/if}> Não
                    </label>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <input type="hidden" name="id" value="{$params.1}">
                <input type="hidden" name="action" value="addOrEditUser">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->