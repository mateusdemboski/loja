<!-- Logo -->
<a href="{$path}/" class="logo"><img src="{$path}/img/logo.png" alt="Catânia Studo"></a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success">{$contactsStared_Inbox|count}</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Você tem {$contactsStared_Inbox|count} mensagens não lidas</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            {section name=c loop=$contactsStared_Inbox max=5}
                            <li><!-- start message -->
                                <a href="{$path}/contatos/ler/{$contactsStared_Inbox[c]['id']}">
                                    <h4>
                                        {$contactsStared_Inbox[c]['name']}
                                        <small><i class="fa fa-clock-o"></i> {$contactsStared_Inbox[c]['date']|date_format:'d/m/y'}</small>
                                    </h4>
                                    <p>{$contactsStared_Inbox[c]['subject']}</p>
                                </a>
                            </li><!-- end message -->
                            {/section}
                        </ul>
                    </li>
                    <li class="footer"><a href="{$path}/contatos">Ver todas as mensagens</a></li>
                </ul>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{$path}/img/avatars/{if $smarty.session.user.image}{$smarty.session.user.image}{else}user.png{/if}" class="user-image" alt="User Image"/>
                    <span class="hidden-xs">{if empty($smarty.session.user.name)}Anônimo{else}{$smarty.session.user.name}{/if}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{$path}/img/avatars/{if $smarty.session.user.image}{$smarty.session.user.image}{else}user.png{/if}" class="img-circle" alt="User Image" />
                        <p>
                            {if empty($smarty.session.user.name)}Anônimo{else}{$smarty.session.user.name}{/if} - {if empty($smarty.session.user.role)}Administrador{else}{$smarty.session.user.role}{/if}
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{$path}/meu-perfil" class="btn btn-default btn-flat">Meu Perfil</a>
                        </div>
                        <div class="pull-right">
                            <a href="{$path}/sair" class="btn btn-default btn-flat">Sair</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>