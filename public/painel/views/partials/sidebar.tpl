<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{$path}/img/avatars/{if $smarty.session.user.image}{$smarty.session.user.image}{else}user.png{/if}" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p>{if empty($smarty.session.user.name)}Anônimo{else}{$smarty.session.user.name}{/if}</p>

            <a href="#">{if empty($smarty.session.user.role)}Administrador{else}{$smarty.session.user.role}{/if}</a>
        </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MENU PRINCIPAL</li>
        <li class="{if $route == 'home'} active{/if}" data-permission-id="1"><a href="{$path}/"><i class="fa fa-dashboard"></i> <span>Página inicial</span></a></li>
        <li class="treeview{if $route == 'banners'} active{/if}">
            <a href="#">
                <i class="fa fa-image"></i>
                <span>Banners</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li{if $params.1 == 1} class="active"{/if}><a href="{$path}/banners/lista/1"><i class="fa fa-image"></i> Home</a></li>
                <li{if $params.1 == 2} class="active"{/if}><a href="{$path}/banners/lista/2"><i class="fa fa-image"></i> Clínica</a></li>
                <li{if $params.1 == 3} class="active"{/if}><a href="{$path}/banners/lista/3"><i class="fa fa-image"></i> Atendimentos</a></li>
                <li{if $params.1 == 4} class="active"{/if}><a href="{$path}/banners/lista/4"><i class="fa fa-image"></i> Equipe</a></li>
                <li{if $params.1 == 5} class="active"{/if}><a href="{$path}/banners/lista/5"><i class="fa fa-image"></i> Primeira Consulta</a></li>
                <li{if $params.1 == 6} class="active"{/if}><a href="{$path}/banners/lista/6"><i class="fa fa-image"></i> Contato</a></li>
            </ul>
        </li>
        <li class="treeview{if $route == 'page'} active{/if}">
            <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Páginas do site</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                {foreach from=$menuPages item=menuPage}
                <li{if $params.0 == $menuPage.slug} class="active"{/if}><a href="{$path}/page/{$menuPage.slug}"><i class="fa fa-file-o"></i> {$menuPage.name}</a></li>
                {/foreach}
            </ul>
        </li>
        <li class="treeview{if $route == 'produtos' || $route == 'cores' || $route == 'tamanhos'} active{/if}">
            <a href="#">
                <i class="fa fa-shopping-cart"></i>
                <span>Produtos</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li{if $route == 'produtos' && $params.0 == 'adicionar'} class="active"{/if}><a href="{$path}/produtos/adicionar"><i class="fa fa-cart-plus"></i> Criar novo produto</a></li>
                <li{if $route == 'produtos' && $params.0 != 'todos'} class="active"{/if}><a href="{$path}/produtos/todos"><i class="fa fa-shopping-cart"></i> Todos os produto</a></li>
                <li{if $route == 'cores'} class="active"{/if}><a href="{$path}/cores/todos"><i class="fa fa-eyedropper"></i> Todos as cores</a></li>
                <li{if $route == 'tamanhos'} class="active"{/if}><a href="{$path}/tamanhos/todos"><i class="fa fa-arrows-v"></i> Todos os tamanhos</a></li>
            </ul>
        </li>

        <li{if $route == 'contatos'} class="active"{/if}>
            <a href="{$path}/contatos">
                <i class="fa fa-envelope"></i> <span>Contatos</span>
                <small class="label pull-right bg-yellow">{$contactsStared_Inbox|count}</small>
            </a>
        </li>

        <li class="treeview{if $route == 'usuarios' || $route == 'funcoes' || $route == 'meu-perfil'} active{/if}">
            <a href="#">
                <i class="fa fa-users"></i>
                <span>Usuários do sistema</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li{if $route == 'usuarios' && $params.0 == 'adicionar'} class="active"{/if}><a href="{$path}/usuarios/adicionar"><i class="fa fa-user-plus"></i> Adicionar Novo</a></li>
                <li{if $route == 'usuarios' && $params.0 == ''} class="active"{/if}><a href="{$path}/usuarios"><i class="fa fa-plus-square"></i> Exibir todos</a></li>
                <li{if $route == 'funcoes'} class="active"{/if}><a href="{$path}/funcoes"><i class="fa fa-briefcase"></i> Funções Admnistrativas</a></li>
                <li{if $route == 'meu-perfil'} class="active"{/if}><a href="{$path}/meu-perfil"><i class="fa fa-user"></i> Editar meu perfil</a></li>
            </ul>
        </li>

        <li class="{if $route == 'configuracoes'} active{/if}"><a href="{$path}/configuracoes"><i class="fa fa-gears"></i> <span>Configurações do Site</span></a></li>
    </ul>
</section>
<!-- /.sidebar -->