<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tamanhos de Produto</h3>
                <a href="{$path}/tamanhos/adicionar" class="btn btn-danger pull-right">Adicionar novo</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Tamanho</th>
                            <th>Ativo</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$sizes item=size}
                        <tr>
                            <td>{$size.id}</td>
                            <td>{$size.size}</td>
                            <td class="text-center"><i class="fa fa-{if $size.active=='N'}times{else}check{/if}-circle"></i> </td>
                            <td class="text-center">
                                <a class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                <a class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Tamanho</th>
                            <th>Ativo</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->