<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Produtos</h3>
                <a href="{$path}/produtos/adicionar" class="btn btn-danger pull-right">Adicionar novo</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Imagem</th>
                            <th>Referência</th>
                            <th>Produto</th>
                            <th>Departamento</th>
                            <th>Preço</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$products item=product}
                        <tr>
                            <td>{$product.id}</td>
                            <td><img style="height:100px;" src="{$site}/images/content/products/{$product.image}"></td>
                            <td>{$product.reference}</td>
                            <td>{$product.name}</td>
                            <td>{$product.department}</td>
                            <td>{$product.price}</td>
                            <td class="text-center">
                                <a class="btn btn-default btn-xs"><i class="fa fa-eye{if $product.active=='N'}-slash{/if}"></i></a>
                                <a href="{$path}/cores/produto/{$product.id}/{$product.department_slug}/{$product.slug}" class="btn btn-default btn-xs">&nbsp;<i class="fa fa-eyedropper"></i>&nbsp;</a>
                                <a href="{$path}/produto-imagens/listar/{$product.id}" class="btn btn-default btn-xs">&nbsp;<i class="fa fa-picture-o"></i>&nbsp;</a>
                                <a href="{$path}/produtos/editar/{$product.id}/{$product.department_slug}/{$product.slug}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                <a class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Imagem</th>
                        <th>Referência</th>
                        <th>Produto</th>
                        <th>Departamento</th>
                        <th>Preço</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->