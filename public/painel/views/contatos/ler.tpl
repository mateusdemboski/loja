{if isset($message)}
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        </div>
    </div>
{/if}
<div class="row">
    <div class="col-md-3">
        <a href="{$path}/contatos/compor" class="btn btn-danger btn-block margin-bottom">Compor</a>
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Pastas</h3>
            </div>
            <div class="box-body no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <li {if $smarty.session.contact_box=='Inbox'}class="active"{/if}><a href="{$path}/contatos/open/Inbox"><i class="fa fa-inbox"></i> Caixa de entrada <span class="label label-primary pull-right">{$contactsStared_Inbox|count}</span></a></li>
                    <li {if $smarty.session.contact_box=='Sent'}class="active"{/if}><a href="{$path}/contatos/open/Sent"><i class="fa fa-envelope-o"></i> Itens Enviados</a></li>
                    <li {if $smarty.session.contact_box=='Outbox'}class="active"{/if}><a href="{$path}/contatos/open/Outbox"><i class="fa fa-sign-out"></i> Caixa de Saída</a></li>
                    <li {if $smarty.session.contact_box=='Trash'}class="active"{/if}><a href="{$path}/contatos/open/Trash"><i class="fa fa-trash-o"></i> Lixeira</a></li>
                </ul>
            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div><!-- /.col -->
    <div class="col-md-9">
        <form class="box box-primary" method="post">
            <div class="box-header with-border">
                <h3 class="box-title">Ler E-mail</h3>
                <div class="box-tools pull-right">
                    <a href="{if $previous.id != 0}{$path}/contatos/ler/{$previous.id}/{$previous.subject|lower|replace:' ':'-'|regex_replace:'/[^A-Za-z0-9\-]/':''|regex_replace:'/-+/':'-'}/{else}javascript:void(0);{/if}" class="btn btn-box-tool" data-toggle="tooltip" title="Anterior"><i class="fa fa-chevron-left"></i></a>
                    <a href="{if $next.id != 0}{$path}/contatos/ler/{$next.id}/{$next.subject|lower|replace:' ':'-'|regex_replace:'/[^A-Za-z0-9\-]/':''|regex_replace:'/-+/':'-'}/{else}javascript:void(0);{/if}" class="btn btn-box-tool" data-toggle="tooltip" title="Próxima"><i class="fa fa-chevron-right"></i></a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="mailbox-read-info">
                    <h3>{$message.subject}</h3>
                    <h5>De: {$message.name} &#60;<a href="mailto:{$message.email}">{$message.email}</a>&#62; <span class="mailbox-read-time pull-right">{$message.date|date_format:'d/m/y'} às {$message.hour|date_format:'H:i'}</span></h5>
                </div><!-- /.mailbox-read-info -->
                <div class="mailbox-controls with-border text-center">
                    <div class="btn-group">
                        <button  data-id="{$message.id}" data-id-show="{if $next.id != 0}{$next.id}{else}{$previous.id}{/if}"  class="btn btn-default btn-sm" data-toggle="tooltip" title="Excluir"><i class="fa fa-trash-o"></i></button>
                        <a href="{$path}/contatos/responder/{$message.id}" class="btn btn-default btn-sm" data-toggle="tooltip" title="Responder"><i class="fa fa-reply"></i></a>
                        <a href="{$path}/contatos/encaminhar/{$message.id}" class="btn btn-default btn-sm" data-toggle="tooltip" title="Encaminhar"><i class="fa fa-share"></i></a>
                    </div><!-- /.btn-group -->
                    <a class="btn btn-default btn-sm" data-toggle="tooltip" title="Imprimir"><i class="fa fa-print"></i></a>
                </div><!-- /.mailbox-controls -->
                <div class="mailbox-read-message">
                    {$message.message}
                </div><!-- /.mailbox-read-message -->
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="pull-right">
                    <a href="{$path}/contatos/responder/{$message.id}" class="btn btn-default"><i class="fa fa-reply"></i> Responder</a>
                    <a href="{$path}/contatos/encaminhar/{$message.id}" class="btn btn-default"><i class="fa fa-share"></i> Encaminhar</a>
                </div>
                <button data-id="{$message.id}" data-id-show="{if $next.id != 0}{$next.id}{else}{$previous.id}{/if}" class="btn btn-default"><i class="fa fa-trash-o"></i> Excluir</button>
                <a href="{$path}/contatos/imprimir/{$message.id}" class="btn btn-default"><i class="fa fa-print"></i> Imprimir</a>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->