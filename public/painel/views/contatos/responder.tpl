{if isset($message)}
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        </div>
    </div>
{/if}
<div class="row">
    <div class="col-md-3">
        <a href="{$path}/contatos/open/Inbox" class="btn btn-danger btn-block margin-bottom">Voltar para Caixa de entrada</a>
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Pastas</h3>
            </div>
            <div class="box-body no-padding">
                <ul class="nav nav-pills nav-stacked">
                    <li {if $smarty.session.contact_box=='Inbox'}class="active"{/if}><a href="{$path}/contatos/open/Inbox"><i class="fa fa-inbox"></i> Caixa de entrada <span class="label label-primary pull-right">{$contactsStared_Inbox|count}</span></a></li>
                    <li {if $smarty.session.contact_box=='Sent'}class="active"{/if}><a href="{$path}/contatos/open/Sent"><i class="fa fa-envelope-o"></i> Itens Enviados</a></li>
                    <li {if $smarty.session.contact_box=='Outbox'}class="active"{/if}><a href="{$path}/contatos/open/Outbox"><i class="fa fa-sign-out"></i> Caixa de Saída</a></li>
                    <li {if $smarty.session.contact_box=='Trash'}class="active"{/if}><a href="{$path}/contatos/open/Trash"><i class="fa fa-trash-o"></i> Lixeira</a></li>
                </ul>
            </div><!-- /.box-body -->
        </div><!-- /. box -->
    </div><!-- /.col -->
    <div class="col-md-9">
        <form class="box box-primary" method="post">
            <div class="box-header with-border">
                <h3 class="box-title">Responder mensagem</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <input class="form-control" placeholder="Para:" value="{$message.email}"/>
                </div>
                <div class="form-group">
                    <input class="form-control" placeholder="Assunto:" value="RES: {$message.subject}"/>
                </div>
                <div class="form-group">
                    <textarea id="compose-textarea" placeholder="Mensagem:" class="form-control" style="height: 300px">
                      <br><hr>
                        <b>De:</b> {$message.name} [{$message.email}]<br>
                        <b>Enviada em:</b> {$message.date|date_format:"%A, %e de %B de %Y"} {$message.hour|date_format:"%H:%M"}<br>
                        <b>Para:</b> Contato - Dr.Berto<br>
                        <b>Assunto:</b> {$message.subject}<br><br>
                        {$message.message}
                    </textarea>
                </div>{*
                <div class="form-group">
                    <div class="btn btn-default btn-file">
                        <i class="fa fa-paperclip"></i> Anexar arquivo
                        <input type="file" name="attachment"/>
                    </div>
                    <p class="help-block">Max. 32MB</p>
                </div>*}
            </div><!-- /.box-body -->
            <div class="box-footer">
                <div class="pull-right">
                    {*<button class="btn btn-default"><i class="fa fa-pencil"></i> Draft</button>*}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Enviar</button>
                </div>
                <button onclick="if(confirm('Deseja realmente descartar esta mensagem?'))window.location='{$path}/contatos/open/Inbox';" class="btn btn-default"><i class="fa fa-times"></i> Descartar</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->