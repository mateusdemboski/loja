{if isset($message)}
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        </div>
    </div>
{/if}
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Cores de produtos</h3>
                <a href="{$path}/cores/adicionar" class="btn btn-danger pull-right">Adicionar novo</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Cor</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$colors item=color}
                    <tr>
                        <td>{$color.id}</td>
                        <td>{$color.name}</td>
                        <td style="background-color:{$color.hex};"><div><span class="sr-only">{$color.hex}</span></div></td>
                        <td class="text-center"><i class="fa fa-{if $color.active=='N'}times{else}check{/if}-circle"></i></td>
                        <td class="text-center">
                            <a href="{$path}/cores/editar/{$color.id}/{$color.slug}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                            <a href="{$path}/cores/remover/{$color.id}/{$color.slug}" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Cor</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->