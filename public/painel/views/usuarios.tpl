<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Usuários do sistema</h3>
                <a href="{$path}/usuarios/adicionar" class="btn btn-danger pull-right">Adicionar novo</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Imagem</th>
                        <th>Nome</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$users item=user}<tr>
                        <td>{$user.id}</td>
                        <td class="text-center"><img style="width:50px;" class="img-circle" src="{$path}/img/avatars/{if $smarty.session.user.image}{$smarty.session.user.image}{else}user.png{/if}"></td>
                        <td>{$user.name}</td>
                        <td class="text-center"><i class="fa fa-{if $user.active=='N'}times{else}check{/if}-circle"></i><span class="sr-only">{if $user.active=='Y'}1{else}0{/if}</span></td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs" data-toggle="tooltip" title="Editar" href="{$path}/usuarios/editar/{$user.id}"><i class="fa fa-pencil-square-o"></i></a>
                            <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="Remover" href="{$path}/usuarios/remover/{$user.id}"><i class="fa fa-times"></i></a>
                        </td>
                        </tr>{/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Imagem</th>
                        <th>Nome</th>
                        <th>Ativo</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->