<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Edição das configurações do site</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                {foreach from=$configurationsSections item=configurationSection}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{$configurationSection.name}</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        {foreach from=$configurationsItems item=configurationItem}{if $configurationItem.id_section == $configurationSection.id}
                        <div class="form-group">
                            <label for="{$configurationItem.name}">{$configurationItem.title}</label>
                            <input class="form-control" name="configuration[{$configurationItem.name}]" id="{$configurationItem.title}" placeholder="{$configurationItem.title}" value="{$configurationItem.value}" maxlength="100" {if $configurationItem.required}required=""{/if}/>
                            {if $configurationItem.help}<p class="help-block">{$configurationItem.help}</p>{/if}
                        </div>
                        {/if}{/foreach}
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                {/foreach}
            </div><!-- /.box-body -->
            <div class="box-footer">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button onclick="location.reload()" class="btn btn-danger"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->