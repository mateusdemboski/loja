<!DOCTYPE html>
<html lang="pt">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="Catânia Studio">
    <meta name="publisher" content="Catânia Studio">
    <title>Catânia Studio | Painel de Controle</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{$path}/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="{$path}/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="{$path}/plugins/iCheck/flat/red.css" rel="stylesheet" type="text/css" />
    <!-- PACE page loader -->
    <link href="{$path}/plugins/pace/pace-theme-minimal.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="{$path}/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="{$path}/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="{$path}/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap colorpicker -->
    <link href="{$path}/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Toastr 2.1.1 -->
    <link href="{$path}/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{$path}/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{$path}/dist/css/skins/skin-red.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-red fixed">
    <div class="wrapper">

        <header class="main-header">
            {include file="../views/partials/header.tpl"}
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            {include file="../views/partials/sidebar.tpl"}
        </aside>

        <!-- Right side column. Contains the navbar and content of the page -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Painel de Controle
                    <small>Versão 2.0</small>
                </h1>
                <ol class="breadcrumb">
                    {foreach from=$breadcrumbs item=breadcrumb}
                    <li{if $breadcrumb.active} class="active"{/if}>
                        {if !$breadcrumb.active}<a href='{$breadcrumb.link}'>{/if}
                            {$breadcrumb.label}
                        {if !$breadcrumb.active}</a>{/if}
                    </li>
                    {/foreach}
                </ol>
            </section>

            <!-- Main content -->
            <section class="content">
                {include file=$mid}
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            {include file="../views/partials/footer.tpl"}
        </footer>

    </div><!-- ./wrapper -->

    <script type="text/javascript">var path='{$path}';var box='{$smarty.session.contact_box}'</script>
    <!-- jQuery 2.1.3 -->
    <script src="{$path}/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{$path}/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="{$path}/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="{$path}/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='{$path}/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="{$path}/dist/js/app.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="{$path}/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="{$path}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="{$path}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="{$path}/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="{$path}/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- bootstrap colorpicker -->
    <script src="{$path}/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
    <!-- CK Editor -->
    <script src="{$path}/plugins/ckeditor/ckeditor.js"></script>
    <script src="{$path}/plugins/ckeditor/adapters/jquery.js"></script>
    <!-- iCheck -->
    <script src="{$path}/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="{$path}/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="{$path}/plugins/chartjs/Chart.min.js" type="text/javascript"></script>
    <!-- ChartJS 1.0.1 -->
    <script src="{$path}/plugins/pace/pace.js" type="text/javascript"></script>
    <!-- MaskMoney 3.0.2 -->
    <script src="{$path}/plugins/maskMoney/jquery.maskMoney.min.js" type="text/javascript"></script>
    <!-- Toastr 2.1.1 -->
    <script src="{$path}/plugins/toastr/toastr.min.js" type="text/javascript"></script>
    <!-- Page script -->
    <script src="{$path}/js/configurations.jquery.js"></script>
    <script src="{$path}/js/functions.jquery.js"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{$path}/dist/js/pages/dashboard2.js" type="text/javascript"></script>
</body>
</html>