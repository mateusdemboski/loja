<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger" enctype="multipart/form-data">
            <div class="box-header with-border">
                <h3 class="box-title">Funções de usuário do sistema <small>Adicionar Novo</small></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Nome" maxlength="50" value="{$name}" required=""/>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
                <input type="hidden" name="id" value="{$params.1}">
                <input type="hidden" name="action" value="addOrEditRole">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->