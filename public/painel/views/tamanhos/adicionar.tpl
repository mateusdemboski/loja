<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger" enctype="multipart/form-data">
            <div class="box-header with-border">
                <h3 class="box-title">Tamanhos <small>Adicionar Novo</small></h3>
            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="form-group">
                    <label for="name">Tamanho</label>
                    <input type="number" class="form-control" name="size" id="size" placeholder="Tamanho" maxlength="2" min="0" max="99" value="{$size.size}" onkeyup="makeSlug(this.value)" required=""/>
                </div><!-- /.form group -->

                <div class="form-group">
                    <label class="control-label">Tamanho ativo?&nbsp;</label>
                    <label class="radio-inline">
                        <input type="radio" name="active" value="Y"{if $size.active != 'N'} checked=""{/if}> Sim
                    </label><!-- /.radio inline -->
                    <label class="radio-inline">
                        <input type="radio" name="active" value="N"{if $size.active == 'N'} checked=""{/if}> Não
                    </label><!-- /.radio inline -->
                </div><!-- /.form group -->

            </div><!-- /.box-body -->

            <div class="box-footer">
                <input type="hidden" name="id" value="{$params.1}">
                <input type="hidden" name="action" value="addOrEditSize">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button class="btn btn-danger" type="reset"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->