<div class="row">
    <div class="col-xs-12">
        {if isset($message)}
            <div class="alert alert-{$message.type}{if $message.dismissible} alert-dismissible{/if}" role="alert">
                {if $message.dismissible}<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>{/if}
                {$message.text}
            </div>
        {/if}
        <form method="post" class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tamanhos de Produto por cor <small>{$productName} - </small></h3>
                <a href="{$path}/cores/produto/{$productID}" class="btn btn-default pull-right"><i class="fa fa-fw fa-chevron-left"></i> Voltar</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                {foreach from=$sizes item=size}
                <div class="form-group">
                    <label for="size{$size.id}">Estoque para o produto {$productName} no tamanho {$size.size}</label>
                    <input class="form-control" name="sizes[{$size.id}]" id="size{$size.id}" value="{$size.stock}" min="0" max="99" type="number"/>
                    <p class="help-block">Deixe vazio para desabilitar</p>
                </div>
                {/foreach}
            </div><!-- /.box-body -->
            <div class="box-footer">
                <input type="hidden" name="id" value="{$params.1}">
                <input type="hidden" name="action" value="updateProductColor_sizes">
                <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Salvar</button>
                <button onclick="location.reload()" class="btn btn-danger"><i class="fa fa-times"></i> Descartar mudanças</button>
            </div><!-- /.box-footer -->
        </form><!-- /. box -->
    </div><!-- /.col -->
</div><!-- /.row -->