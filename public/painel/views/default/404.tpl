<div class="error-page">
    <h2 class="headline text-yellow"> 404</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Página não encontrada.</h3>
        <p>
            Não foi possível encontrar a página que você estava procurando.
            {*Entretanto, você pode <a href='{$path}/'>voltar para a página inicial</a> ou tentar fazer uma busca.*}
        </p>
        {*<form class='search-form'>
            <div class='input-group'>
                <input type="text" name="search" class='form-control' placeholder="Estou procurando por:"/>
                <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i></button>
                </div>
            </div><!-- /.input-group -->
        </form>*}
    </div><!-- /.error-content -->
</div><!-- /.error-page -->