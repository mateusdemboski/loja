{* MetaTags - Para indexar melhor no Google - Editar os campos que forem necessários *}
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="{$path}/img/icone.ico" />
<meta name="author" content="Catânia Studio" />
<meta name="Publisher" content="Catânia Studio" />
<meta name="owner" content="Catânia Studio" />
<meta name="copyright" content="Catânia Studio © 2015" />
<meta name="robots" content="noindex,nofollow" />
<meta http-equiv="content-language" content="Português" />