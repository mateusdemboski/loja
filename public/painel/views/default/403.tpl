<div class="error-page">
    <h2 class="headline text-yellow"> 403</h2>
    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Acesso Negado!</h3>
        <p>
            Não foi possível accesar esta página devido a sua função no sistema.<br>
            Caso necessite, entre em contato com um administrador do sistema e solicite a autorização!
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->