<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Hover Data Table</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Nome</th>
                            <th>Slug</th>
                            <th>Valor</th>
                            <th>Tipo</th>
                            <th>Destinado para</th>
                            <th>Válidade</th>
                            <th>Ativo</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>01</td>
                            <td>blábláblá</td>
                            <td>blablabla</td>
                            <td>10</td>
                            <td>Porcentagem</td>
                            <td>Todos</td>
                            <td>02/03/2017</td>
                            <td class="text-center"><i class="fa fa-check-circle"></i> </td>
                            <td class="text-center">
                                <a class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>
                                <a class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                <a class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td>02</td>
                            <td>bláblábláblá</td>
                            <td>blablablabla</td>
                            <td>15</td>
                            <td>Porcentagem</td>
                            <td><a>Mateus Demboski</a></td>
                            <td>02/02/2017</td>
                            <td class="text-center"><i class="fa fa-times-circle"></i> </td>
                            <td class="text-center">
                                <a class="btn btn-default btn-xs"><i class="fa fa-eye-slash"></i></a>
                                <a class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                <a class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#ID</th>
                            <th>Nome</th>
                            <th>Slug</th>
                            <th>Valor</th>
                            <th>Tipo</th>
                            <th>Destinado para</th>
                            <th>Válidade</th>
                            <th>Ativo</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->