<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Funções dos usuários do sistema</h3>
                <a href="{$path}/funcoes/adicionar" class="btn btn-danger pull-right">Adicionar novo</a>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$roles item=role}<tr>
                        <td>{$role.id}</td>
                        <td>{$role.name}</td>
                        <td class="text-center">
                            <a class="btn btn-default btn-xs" data-toggle="tooltip" title="Permições" href="{$path}/funcoes/permicoes/{$role.id}"><i class="fa fa-lock"></i></a>
                            <a class="btn btn-default btn-xs" data-toggle="tooltip" title="Editar" href="{$path}/funcoes/editar/{$role.id}"><i class="fa fa-pencil-square-o"></i></a>
                            <a class="btn btn-danger btn-xs" data-toggle="tooltip" title="Remover" href="{$path}/funcoes/remover/{$role.id}"><i class="fa fa-times"></i></a>
                        </td>
                        </tr>{/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Nome</th>
                        <th>Ações</th>
                    </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div><!-- /.row -->