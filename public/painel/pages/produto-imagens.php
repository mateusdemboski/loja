<?php

if(isPost()):

    if($_POST['action'] == 'addOrEditProductImage'){

        if ( !empty( $_FILES['photo']['name'] ) ) {

            $name      = explode( '.' , $_FILES['photo']['name'] );
            $tempPath  = $_FILES['photo']['tmp_name'];
            $extension = array_pop( $name );
            $name      = implode( '.' , $name );
            $original  = $name.'.'.$extension;

            $length = rand(0,32);
            $start  = rand(0,$length);

            $fileName = clean($name) . '-' . substr( MD5( uniqid( rand(), true ) ) , $start, $length ) . '.' . $extension;

            $uploadPath = $configurations['UPLOAD_DIRECTORY'] . DS . 'products' . DS . $fileName;

            if(move_uploaded_file( $tempPath, $uploadPath )){
                //exclui o arquivo anterior, caso ele existir
                if(file_exists($configurations['UPLOAD_DIRECTORY'].DS.'products'.DS.$_POST['photoOriginal']) &&
                    !is_dir($configurations['UPLOAD_DIRECTORY'].DS.'products'.DS.$_POST['photoOriginal'])
                ) unlink($configurations['UPLOAD_DIRECTORY'].DS.'products'.DS.$_POST['photoOriginal']);

                //Base miníma para imagem
                $base = array( 'width' => 560 , 'height' => 840 );

                //lê a imagem com o Imagine usando a biblioteca GD caso o Imagick não esteja instalado
                $imagine = extension_loaded('imagick') ? new Imagine\Imagick\Imagine() : new Imagine\Gd\Imagine();
                $uploadedImage = $imagine->open($uploadPath);

                //Pega o tamanho da imagem
                $imageSize = $uploadedImage->getSize();

                $_imgx = $imageSize->getWidth();
                $_imgy = $imageSize->getHeight();

                //Atualiza os valores da base para os calculos.
                if($_imgx > $base['width'] || $_imgy > $base['height']){
                    if($_imgx > $base['width']){
                        $base['height'] = round( ( $_imgx * $base['height'] ) / $base['width'] );
                        $base['width']  = $_imgx;
                    }else{
                        $base['width']  = round( ( $_imgy * $base['width'] ) / $base['height'] );
                        $base['height'] = $_imgy;
                    }
                }// endif

                //se algum dos lados da base for menor que a imagem, a imagem original é cortada
                //para adaptarse ao valor da base.
                if($_imgx > $base['width'] || $_imgy > $base['height'])
                    $uploadedImage->crop(new Imagine\Image\Point(0, 0),new Imagine\Image\Box($base['width'],$base['height']));

                $position = calc_position($base, $_imgx, $_imgy);

                //Cria a base para imagem
                $collage = $imagine->create(new Imagine\Image\Box($base['width'],$base['height']));
                //cola a imagem original na base, centralizando-a
                $collage->paste($uploadedImage, new Imagine\Image\Point($position['x'], $position['y']));
                //sobrescreve a imagem original com a nova
                $collage->save($uploadPath);
            }// endif
        } else $fileName = $_POST['photoOriginal'];

        $id        = (int) $_POST['id'];

        $imageData = [
            'title'        => $_POST['title'],
            'name'         => $fileName,
            'original'     => $original,
            'description'  => $_POST['description'],
            'table'        => 'products',
            'id_reference' => $_POST['id_reference'],
            'active'       => $_POST['active']
        ];

        if(empty($id)){

            $createImage = $database->insert('images', $imageData);

            if ($createImage) $message = [
                'type' => 'success',
                'text' => 'Imagem adicionada com sucesso!'];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];
        }else{

            $updateImage = $database->update('images', $imageData, array('id'=>$id));

            if ($updateImage) $message = [
                'type' => 'success',
                'text' => 'Imagem atualizada com sucesso.'];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

        }// endif
    }else $message = [
        'type'=>'danger',
        'text'=>'<strong>Oops, Parece que houve um erro</strong>: A ação solicitada é inválida!'
    ]; // endif

    $smarty->assign('message', $message);
endif;

$queryProductReferenceName = $database -> createQueryBuilder();
$queryProductReferenceName -> select('name')
                        -> from('products')
                        -> where('id=:idProduct')
                        -> setParameter(':idProduct',$params[1],\PDO::PARAM_INT);
$productReferenceNameStatement = $queryProductReferenceName->execute();
$productReferenceName = $productReferenceNameStatement->fetch(\PDO::FETCH_ASSOC);
$smarty->assign('productReferenceName', $productReferenceName['name']);

switch($action){
    case 'adicionar':

        $displayTpl = 'produto-imagens/adicionar';
        break;

    case 'editar':

        $displayTpl = 'produto-imagens/adicionar';
        $queryImage = $database -> createQueryBuilder();
        $queryImage -> select('title, name, original, description, active')
                    -> from('images')
                    -> where('id=:idImage')
                    -> setParameter(':idImage',$params[2],\PDO::PARAM_INT);
        $imageStatement = $queryImage->execute();

        $image = $imageStatement->fetch(\PDO::FETCH_ASSOC);
        $smarty->assign('title',$image['title']);
        $smarty->assign('name',$image['name']);
        $smarty->assign('original',$image['original']);
        $smarty->assign('description',$image['description']);
        $smarty->assign('active',$image['active']);
        break;

    case 'remover':

        $idBanner        = (int) $params[2];
        $bannerDataQuery = $database->createQueryBuilder();
        $bannerDataQuery -> select('id, name')
                         -> from('images')
                         -> where('id=:idBanner')
                         -> setParameter(':idBanner',$idBanner);
        $bannerDataStatement = $bannerDataQuery->execute();
        $bannerData = $bannerDataStatement->fetch(\PDO::FETCH_ASSOC);
        $bannerPhotoPath = $configurations['UPLOAD_DIRECTORY'] . DS . 'banner' . DS . $bannerData['name'];

        if($bannerData['id'] == $idBanner){
            $excludedBanner = $database->delete('images',['id'=> $bannerData['id']]);
            if(file_exists($bannerPhotoPath) && !is_dir($bannerPhotoPath))
                unlink($bannerPhotoPath);

            if ($excludedBanner) $message = [
                'type' => 'success',
                'text' => "Banner removido com sucesso."];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

        }else $message = [
            'type' => 'danger',
            'text' => '<strong>Oops, Parece que houve um erro...</strong> O banner selecionado é inválido!'];

        $imagesQuery = $database->createQueryBuilder();
        $imagesQuery -> select('id, title, name, id_reference, active')
                     -> from('images')
                     -> where('`table`="banners" AND id_reference=:idReference')
                     -> setParameter(':idReference',$params[1]);
        $imagesStatement = $imagesQuery->execute();
        $images = $imagesStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('images', $images);
        break;

    default:

        $reference = (int) $params[1];
        if($reference == 0) $reference = 1;

        $imagesQuery = $database->createQueryBuilder();
        $imagesQuery -> select('id, title, name, id_reference, active')
            -> from('images')
            -> where('`table`="products" AND id_reference=:idReference')
            -> setParameter(':idReference',$reference,\PDO::PARAM_INT);
        $imagesStatement = $imagesQuery->execute();
        $images = $imagesStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('images', $images);
        break;
}