<?php
if(isPost()):
    $id = (int) $_POST['id'];
    if($_POST['action'] == 'updateProductColor_sizes'){
        $checking = array();
        $sizes = (array) $_POST['sizes'];
        $emptyTable = $database->delete('sizes_by_product_color',['id_product_color'=>$id],['id_product_color'=>\PDO::PARAM_INT]);

        foreach($sizes as $idSize => $sizeStock)
            $checking[] = $database->insert(
                'sizes_by_product_color',
                [
                    'id_product_color' => $id,
                    'id_size'=> $idSize,
                    'stock'=> $sizeStock
                ],
                ['id_product_color' => \PDO::PARAM_INT, 'id_size'=> \PDO::PARAM_INT, 'stock'=> \PDO::PARAM_INT]
            );

        if(in_array(0,$checking))$message = [
            'type'=>'danger',
            'text'=>'<strong>Oops, Parece que houve um erro...</strong> Parece que ao menos um dos tamanhos não foi gravada corretamente. Por favor, tente novamente. Caso este erro persista, entre em contato conosco!'
        ];

        else $message = [
            'type'=>'success',
            'text'=>'<strong>Tamanhos para o produto por cor atualizadas com sucesso!</strong>'
        ];
    }else{
        $namedAction = empty($id) ? 'adiciona': 'altera';
        $data        = array(
            'size'        => $_POST['size'],
            'active'      => $_POST['active']
        );
        $make = empty($id) ? $database->insert('sizes', $data) : $database->update('sizes',$data,['id'=>$id]);
        if($make) $message = array('type'=>'success', 'dismissible'=>true, 'text'=>"Dados {$namedAction}dos com sucesso!");
        else $message = array('type'=>'danger', 'dismissible'=>true, 'text'=>"<strong>Oops! Parece que houve um erro...</strong> Por Favor, tente novamente {$namedAction}r os dados. Caso este erro persista, entre em contato com o nosso suporte: <a href='mailto:programacao@cataniastudio.com?cc=laci@cataniastudio.com&subject=Preciso de Ajuda! (Painel - Boutique Werner)&body=Descreva seu problema com máximo de detalhes possível!'>programacao@cataniastudio.com</a>");
    }

    $smarty->assign('message', $message);
endif;

switch($action){
    case 'adicionar': $displayTpl = 'tamanhos/adicionar'; break;
    case 'editar':
        $displayTpl = 'tamanhos/adicionar';

        $sizeQuery = $database -> createQueryBuilder();
        $sizeQuery -> select('id, size, active')
                   -> from('sizes')
                   -> where('id=:idSize')
                   -> setParameter(':idSize',$params[1],\PDO::PARAM_INT);
        $sizeStatement = $sizeQuery->execute();

        $size = $sizeStatement->fetch(\PDO::FETCH_ASSOC);
        $smarty->assign('size', $size);
        break;

    case 'porcordeproduto':
        $displayTpl = 'tamanhos/porcordeproduto';
        $idProductColor = $params[1];

        $sizesQuery = $database -> createQueryBuilder();
        $sizesQuery -> select('s.id, s.size, sbpc.stock')
            -> from('sizes','s')
            -> leftJoin('s','sizes_by_product_color','sbpc','s.id=sbpc.id_size')
            -> where('s.active=:isActive')
            -> setParameter(':isActive','Y');
        $sizesStatement = $sizesQuery->execute();

        $sizes = $sizesStatement->fetchAll(\PDO::FETCH_ASSOC);
        $smarty->assign('sizes', $sizes);

        $getProductNameQuery  = $database -> createQueryBuilder();
        $getProductNameQuery -> select('p.id, p.name')
            -> from('products','p')
            -> innerJoin('p','product_colors','pc','pc.id_product = p.id')
            -> where('pc.id=:idProductColor')
            -> setParameter(':idProductColor',$idProductColor,\PDO::PARAM_INT);
        $getProductNameStatement = $getProductNameQuery -> execute();
        $getProductName = $getProductNameStatement->fetch(\PDO::FETCH_ASSOC);

        $smarty->assign('productName',$getProductName['name']);
        $smarty->assign('productID',$getProductName['id']);

        break;

    default:
        $sizesQuery = $database -> createQueryBuilder();
        $sizesQuery -> select('id, size, active') -> from('sizes');
        $sizesStatement = $sizesQuery->execute();

        $sizes = $sizesStatement->fetchAll(\PDO::FETCH_ASSOC);
        $smarty->assign('sizes', $sizes);
        break;
}