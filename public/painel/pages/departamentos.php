<?php
if(isPost()):

    $id          = (int) $_POST['id'];
    $namedAction = empty($id) ? 'adiciona': 'altera';
    $data        = array(
        'name'   => $_POST['name'],
        'slug'   => $_POST['slug'],
        'active' => $_POST['active']
    );
    $make = empty($id) ? $database->insert('departments', $data) : $database->update('departments',$data,['id'=>$id]);
    if($make) $message = array('type'=>'success', 'dismissible'=>true, 'text'=>"Dados {$namedAction}dos com sucesso!");
    else $message = array('type'=>'danger', 'dismissible'=>true, 'text'=>"<strong>Oops! Parece que houve um erro...</strong> Por Favor, tente novamente {$namedAction}r os dados. Caso este erro persista, entre em contato com o nosso suporte: <a href='mailto:programacao@cataniastudio.com?cc=laci@cataniastudio.com&subject=Preciso de Ajuda! (Painel - Boutique Werner)&body=Descreva seu problema com máximo de detalhes possível!'>programacao@cataniastudio.com</a>");

    $smarty->assign('message', $message);
endif;

switch($action){
    case 'adicionar': $displayTpl = 'departamentos/adicionar'; break;

    case 'editar':
        $displayTpl = 'departamentos/adicionar';
        $departmentQuery = $database -> createQueryBuilder();
        $departmentQuery -> select('id, name, slug, active')
                         -> from('departments')
                         -> where('id=:idDepartment')
                         -> setParameter(':idDepartment',$params[1], \PDO::PARAM_INT);
        $departmentStatement = $departmentQuery->execute();

        $department = $departmentStatement->fetch(\PDO::FETCH_ASSOC);
        $smarty->assign('department', $department);
        break;

    default:
        $departmentsQuery = $database -> createQueryBuilder();
        $departmentsQuery -> select('id, name, slug, active') -> from('departments');
        $departmentsStatement = $departmentsQuery->execute();

        $departments = $departmentsStatement->fetchAll(\PDO::FETCH_ASSOC);
        $smarty->assign('departments', $departments);
        break;
}