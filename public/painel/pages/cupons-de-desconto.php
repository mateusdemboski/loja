<?php
$couponsQuery = $database -> createQueryBuilder();
$couponsQuery -> select('id, name, slug, value, type, assigned_for, valid_until, active') -> from('coupons');
$couponsStatement = $couponsQuery->execute();
$smarty->assign('coupons',$couponsStatement->fetchAll(\PDO::FETCH_ASSOC));