<?php

if(isPost() && isset($_POST['confirm'])){
    session_destroy();
    header("Location: {$path}/login.php?logout");
}