<?php

if(isPost()):

    if($_POST['action'] == 'addOrEditRole'){

        $id       = (int) $_POST['id'];
        $roleData = [ 'name' => $_POST['name'], ];

        if(empty($id)){

            $createRole = $database->insert('roles', $roleData);
            $idRole = $database->lastInsertID();

            if ($createRole) $message = [
                'type' => 'success',
                'text' => 'Função adicionada com sucesso ao sistema. Clique <a class="alert-link" href="' . $path . '/funcoes/permicoes/' . $idRole . '">aqui</a> para editar as permições.'];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];
        }else{

            $updateRole = $database->update('roles', $roleData, ['id'=>$id]);

            if ($updateRole) $message = [
                'type' => 'success',
                'text' => 'Nome da Função atualizado com sucesso.'];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

        }// endif
    }elseif($_POST['action'] == 'updatePermissions'){
        $checking    = array();
        $permissions = (array) $_POST['permissions'];

        $database->delete('role_permissions',['id_role'=>$_POST['id']],['id_role'=>\PDO::PARAM_INT]);

        foreach($permissions as $permission)
            $checking[] = $database->insert(
                'role_permissions',
                [
                    'id_role'=>(int) $_POST['id'],
                    'id_permission'=>(int) $permission
                ]
            );

        if(in_array(0,$checking))$message = [
            'type'=>'danger',
            'text'=>'<strong>Oops, Parece que houve um erro...</strong> Parece que ao menos uma das permições selecionadas não foi gravada corretamente. Por favor, tente novamente. Caso este erro persista, entre em contato conosco!'
        ];

        else $message = [
            'type'=>'success',
            'text'=>'<strong>Permições atualizadas com sucesso!</strong>'
        ];


    }else $message = [
        'type'=>'danger',
        'text'=>'<strong>Oops, Parece que houve um erro</strong>: A ação solicitada é inválida!'
    ]; // endif

    $smarty->assign('message', $message);
endif;

switch($action){
    case 'adicionar':
        $displayTpl = 'funcoes/adicionar';
        break;

    case 'editar':
        $displayTpl = 'funcoes/adicionar';

        $roleQuery = $database -> createQueryBuilder();
        $roleQuery -> select('name') -> from('roles') -> where('id=:idRole') -> setParameter(':idRole',$params[1],\PDO::PARAM_INT);
        $roleStatement = $roleQuery->execute();

        $role = $roleStatement->fetch(\PDO::FETCH_ASSOC);

        $smarty->assign('name',$role['name']);

        break;
    case 'permicoes':
        $displayTpl = 'funcoes/permicoes';

        $role_permissionsQuery = $database->createQueryBuilder();
        $role_permissionsQuery -> select('id_permission')
                               -> from('role_permissions')
                               -> where('id_role=:idRole')
                               -> setParameter(':idRole',$params[1],\PDO::PARAM_INT);
        $role_permissionsStatement = $role_permissionsQuery->execute();
        $rolePermissions = array();
        while($row = $role_permissionsStatement->fetch(PDO::FETCH_ASSOC)) $rolePermissions[] = $row['id_permission'];
        $smarty->assign('role_permissions', $rolePermissions);

        $permissionsQuery = $database -> createQueryBuilder();
        $permissionsQuery -> select('id, name') -> from('permissions') -> orderBy('`order` DESC, name');
        $permissionsStatement = $permissionsQuery->execute();

        $permissions = $permissionsStatement->fetchAll(\PDO::FETCH_ASSOC);
        $smarty->assign('permissions', $permissions);

        break;
    case 'remover':

        $idRole = (int) $params[1];

        $removeRolePermissions = $database->delete('role_permissions',['id_role'=>$idRole],['id_role'=>\PDO::PARAM_INT]);
        $removeRole = $database->delete('roles',['id'=>$idRole],['id'=>\PDO::PARAM_INT]);

        $rolesQuery = $database -> createQueryBuilder();
        $rolesQuery -> select('id, name') -> from('roles');
        $rolesStatement = $rolesQuery->execute();

        if($removeRole)$message = [
            'type'=>'danger',
            'text'=>'<strong>Oops, Parece que houve um erro...</strong> Parece que ao menos uma das permições selecionadas não foi gravada corretamente. Por favor, tente novamente. Caso este erro persista, entre em contato conosco!'
        ];

        else $message = [
            'type'=>'success',
            'text'=>'<strong>Permições atualizadas com sucesso!</strong>'
        ];

        $roles = $rolesStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('roles', $roles);
        break;
    default:

        $rolesQuery = $database -> createQueryBuilder();
        $rolesQuery -> select('id, name') -> from('roles');
        $rolesStatement = $rolesQuery->execute();

        $roles = $rolesStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('roles', $roles);
        break;
}