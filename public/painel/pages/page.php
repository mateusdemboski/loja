<?php
if(isPost()):
    $slug    = clean($params[0]);

    $data    = array(
        'name'        => $_POST['name'],
        'text'        => $_POST['text'],
        'description' => $_POST['description'],
        'active'      => $_POST['active']
    );
    $updated = $database->update('pages',$data,['slug'=>$slug]);
    if($updated) $message = array('type'=>'success', 'dismissible'=>true, 'value'=>"Dados alterados com sucesso!");
    else $message = array('type'=>'danger', 'dismissible'=>true, 'value'=>"<strong>Oops! Parece que houve um erro...</strong> Por Favor, tente novamente efetuar a alteração. Caso este erro persista, entre en contato com o nosso suporte: <a href='mailto:programacao@cataniastudio.com?cc=laci@cataniastudio.com&subject=Preciso de Ajuda! (Painel - Boutique Werner)&body=Descreva seu problema com máximo de detalhes possível!'>programacao@cataniastudio.com</a>");

    $smarty->assign('message', $message);
endif;
$pageQuery = $database -> createQueryBuilder();
$pageQuery -> select('name, text, description, active')
           -> from('pages')
           -> where('slug=:requestedSlug')
           -> setParameter(':requestedSlug',$params[0],\PDO::PARAM_STR);
$pageStatement = $pageQuery->execute();

$page = $pageStatement->fetch(\PDO::FETCH_ASSOC);
$smarty->assign('page', $page);