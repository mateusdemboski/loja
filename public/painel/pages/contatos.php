<?php
if(!isset($_SESSION['contact_box']) || empty($_SESSION['contact_box'])) $_SESSION['contact_box'] = 'Inbox';

if(isPost()):

    if($_POST['action'] === 'sendmail'){

        $emailConfigurationsQuery = $database -> createQueryBuilder();
        $emailConfigurationsQuery -> select('value') -> from('configurations') -> where('id_section=1');
        $emailConfigurationsStatement = $emailConfigurationsQuery->execute();
        $emailConfigurations          = $emailConfigurationsStatement->fetch(\PDO::FETCH_ASSOC);

        $mail = new PHPMailer();
        $mail -> isSMTP();
        $mail -> SMTPAuth = true;                                                 // Enable SMTP authentication
        $mail -> Host = $emailConfigurations['host'];                             // Set the SMTP Host to connection
        $mail -> SMTPSecure = $emailConfigurations['smtp_secure'];                // Enable SSL secure connection
        $mail -> Port = $emailConfigurations['port'];                             // SMTP SSL Port: 465 | Non Secure: 587
        $mail -> CharSet = 'UTF-8';                                               // Message Charset, default is UTF-8
        $mail -> Username = $emailConfigurations['username'];                     // SMTP username
        $mail -> Password = $emailConfigurations['password'];                     // SMTP password
        $mail -> setLanguage('pt', VENDOR_DIR.DS.'phpmailer'.DS.'phpmailer'.DS.'language'.DS);

        $mail -> From = $emailConfigurations['username'];                         // The From email address for the message
        $mail -> FromName = $emailConfigurations['name'];                         // The From name of the message.
        $mail -> addReplyTo($emailConfigurations['reply_to'],$emailConfigurations['name']);
        $mail -> addAddress($_POST['email']);                                     // Add a recipient

        $mail -> isHTML(true);                                                    // Set email format to HTML

        $mail -> Subject = $_POST['subject'];
        $mail -> Body    = $_POST['body'];
        $mail -> AltBody = 'Para você poder ler está mensagem você deve utilizar um cliente de email que aceite mensagens em HTML.';

        if(!$mail -> send()) {
            $message = array('type'=>'danger','title'=>'Ops... Parece que houve um erro!','value'=>'Infelizmente não foi possivel envir seu contato, por favor, tente novamente. Se o problema persistir entre em contato conosco via e-mail <a href="mailto:contato@ drberto.com.br?subject=[Contato] Dr. Berto (Não consegui enviar meu contato pelo site)&body=Olá, Recebi o seguinte erro ao tentar enviar um contato pelo seu site (www.drberto.com.br):'."\r\n\r\n $mail->ErrorInfo".' ">contato@drberto.com.br</a>!<br><small><strong>Erro:</strong>'.$mail->ErrorInfo.'</small>');
        } else {
            $message = array('type'=>'success','title'=>'Sucesso!','value'=>'Seu contato foi enviado com sucesso!');
        }

        $mail -> ClearAllRecipients();
        $mail -> ClearAttachments();
    }

endif;
/*
   $emailConfigurationsQuery = $database -> createQueryBuilder();
   $emailConfigurationsQuery -> select('value') -> from('configurations') -> where('id_section=1');
   $emailConfigurationsStatement = $emailConfigurationsQuery->execute();
   $emailConfigurations          = $emailConfigurationsStatement->fetch(\PDO::FETCH_ASSOC);
 */
switch($action){
    case 'compor':
        $displayTpl = 'contatos/compor';
        break;

    case 'ler':
        $displayTpl = 'contatos/ler';
        $idContact  = (int) $params[1];

        //mark the massage as read
        $readMessage = $database->update('contacts',['stared'=>'N'],['id'=>$idContact]);

        $previousQuery = $database -> createQueryBuilder();
        $previousQuery -> select('id, subject') -> from('contacts') -> where('id<:idContact') -> setParameter(':idContact',$idContact,\PDO::PARAM_INT);
        $previousStatement  = $previousQuery->execute();
        $previous           = $previousStatement->fetch(\PDO::FETCH_ASSOC);

        $messageQuery = $database -> createQueryBuilder();
        $messageQuery -> select('id, name, email, phone, city, subject, message, date, hour, box, stared')
                      -> from('contacts') -> where('id=:idContact') -> setParameter(':idContact',$idContact,\PDO::PARAM_INT);
        $messageStatement  = $messageQuery->execute();
        $message           = $messageStatement->fetch(\PDO::FETCH_ASSOC);

        $nextQuery = $database -> createQueryBuilder();
        $nextQuery -> select('id, subject') -> from('contacts') -> where('id>:idContact') -> setParameter(':idContact',$idContact,\PDO::PARAM_INT);
        $nextStatement  = $nextQuery->execute();
        $next           = $nextStatement->fetch(\PDO::FETCH_ASSOC);

        $smarty->assign('previous', $previous);
        $smarty->assign('message' , $message);
        $smarty->assign('next'    , $next);

        break;

    case 'encaminhar':
        $displayTpl = 'contatos/encaminhar';
        $idContact  = (int) $params[1];

        //mark the massage as read
        $readMessage = $database->update('contacts',['stared'=>'N'],'id='.$idContact);

        $message = $database->read('contacts',['id', 'name', 'email', 'phone', 'city', 'subject', 'message', 'date', 'hour', 'box', 'stared'],'id=:idContact',['idContact'=>$idContact],'id',1);

        $smarty->assign('message' , $message);

        break;

    case 'responder':
        $displayTpl = 'contatos/responder';
        $idContact  = (int) $params[1];

        //mark the massage as read
        $readMessage = $database->update('contacts',['stared'=>'N'],'id='.$idContact);

        $message = $database->read('contacts',['id', 'name', 'email', 'phone', 'city', 'subject', 'message', 'date', 'hour', 'box', 'stared'],'id=:idContact',['idContact'=>$idContact],'id',1);

        $smarty->assign('message' , $message);

        break;

    case 'remover':

        $idContact = (int) $params[1];

        $messageRemoved = $database->delete('contacts',['id'=>$idContact]);

        if($messageRemoved) $message = [
            'type' => 'success',
            'text' => "Contato removido com sucesso."];

        else $message = [
            'type' => 'danger',
            'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

        $smarty->assign('message', $message);

        $contactsQuery = $database -> createQueryBuilder();
        $contactsQuery -> select('id , name, date, subject, stared')
                       -> from('contacts')
                       -> where('box=:boxName')
                       -> orderBy('stared, date','DESC')
                       -> setParameter(':boxName',$_SESSION['contact_box'],\PDO::PARAM_STR);
        $contactsStatement  = $contactsQuery->execute();
        $contacts           = $contactsStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('contacts',$contacts);
        break;
    case 'lixo':

        $idContact = (int) $params[1];

        $messageMove = $database->update('contacts',['box'=>'Trash'],['id'=>$idContact]);

        if($messageMove) $message = [
            'type' => 'success',
            'text' => "Contato movido para lixeira."];

        else $message = [
            'type' => 'danger',
            'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

        $smarty->assign('message', $message);

        $contactsQuery = $database -> createQueryBuilder();
        $contactsQuery -> select('id , name, date, subject, stared')
                       -> from('contacts')
                       -> where('box=:boxName')
                       -> orderBy('stared, date','DESC')
                       -> setParameter(':boxName',$_SESSION['contact_box'],\PDO::PARAM_STR);
        $contactsStatement  = $contactsQuery->execute();
        $contacts           = $contactsStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('contacts',$contacts);
        break;

    case 'open':
            $box = $params[1];
            if(!in_array($box,array('Inbox','Outbox','Sent','Trash'))) $box = 'Inbox';
            $_SESSION['contact_box'] = $box;
            header("Location: {$path}/contatos/");
        break;

    default:
        $contactsQuery = $database -> createQueryBuilder();
        $contactsQuery -> select('id , name, date, subject, stared')
                       -> from('contacts')
                       -> where('box=:boxName')
                       -> orderBy('stared, date','DESC')
                       -> setParameter(':boxName',$_SESSION['contact_box'],\PDO::PARAM_STR);
        $contactsStatement  = $contactsQuery->execute();
        $contacts           = $contactsStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('contacts',$contacts);
        break;
}