<?php

if(isPost()):

    $newConfigurations = $_POST['configuration'];
    $successfulUpdates = array();
    foreach($newConfigurations as $newConfigurationName => $newConfigurationValue)
        $successfulUpdates[] = $database->update('configurations',['value'=>$newConfigurationValue],['name'=>$newConfigurationName]);

    if(in_array(1,$successfulUpdates)) $message = [
        'type' => 'success',
        'text' => 'Configurações atualizadas com sucesso!'];

    else $message = [
        'type' => 'danger',
        'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

    $smarty->assign('message', $message);

endif;

$configurationsSectionsQuery = $database -> createQueryBuilder();
$configurationsSectionsQuery -> select('id, name')
                             -> from('configuration_sections')
                             -> where('active=:isActive')
                             -> setParameter(':isActive','Y')
                             -> orderBy('`order`');
$configurationsSectionsStatement = $configurationsSectionsQuery->execute();

$configurationsSections = $configurationsSectionsStatement->fetchAll(\PDO::FETCH_ASSOC);

$configurationsItemsQuery = $database -> createQueryBuilder();
$configurationsItemsQuery -> select('id, name, title, value, help, required, id_section')
                          -> from('configurations')
                          -> where('active=:isActive')
                          -> setParameter(':isActive','Y')
                          -> orderBy('`order`');
$configurationsItemsStatement = $configurationsItemsQuery->execute();

$configurationsItems = $configurationsItemsStatement->fetchAll(\PDO::FETCH_ASSOC);

$smarty->assign('configurationsSections', $configurationsSections);
$smarty->assign('configurationsItems'   , $configurationsItems);