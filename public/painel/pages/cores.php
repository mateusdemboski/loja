<?php
if(isPost()):
    $id =  $_POST['id'];

    if($_POST['action'] == 'updateProductColors'){
        $checking = [];
        $colors   = (array) $_POST['colors'];

        $ids = [];
        foreach($colors as $color) if(is_numeric($color)) $ids[]=$color;

        $emptyTableQuery = $database->createQueryBuilder();
        $emptyTableQuery -> delete('product_colors')
                         -> where('id_product = :idProduct AND id_color NOT IN (:ids)')
                         -> setParameters([':idProduct'=>$id,':ids'=>implode(',',$ids)]);

        $emptyTable = $emptyTableQuery->execute();

        foreach($colors as $color)
            $checking[] = $database->insert(
                'product_colors',
                [
                    'id_product'=> $id,
                    'id_color'=> $color
                ],
                ['id_product'=>\PDO::PARAM_INT,'id_color'=>\PDO::PARAM_INT]
            );

        if(in_array(0,$checking))$message = [
            'type'=>'danger',
            'text'=>'<strong>Oops, Parece que houve um erro...</strong> Parece que ao menos uma das cores selecionadas não foi gravada corretamente. Por favor, tente novamente. Caso este erro persista, entre em contato conosco!'
        ];

        else $message = [
            'type'=>'success',
            'text'=>'<strong>As cores para o produto foram atualizadas com sucesso!</strong>'
        ];
    }else{
        $namedAction = empty($id) ? 'adiciona': 'altera';
        $data        = array(
            'name'   => $_POST['name'],
            'slug'   => $_POST['slug'],
            'hex'    => $_POST['hex'],
            'active' => $_POST['active']
        );
        $make = empty($id) ? $database->insert('colors', $data) : $database->update('colors',$data,['id'=>$id],['id'=>\PDO::PARAM_INT]);
        if($make) $message = array('type'=>'success', 'dismissible'=>true, 'text'=>"Dados {$namedAction}dos com sucesso!");
        else $message = array('type'=>'danger', 'dismissible'=>true, 'text'=>"<strong>Oops! Parece que houve um erro...</strong> Por Favor, tente novamente {$namedAction}r os dados. Caso este erro persista, entre em contato com o nosso suporte: <a href='mailto:programacao@cataniastudio.com?cc=laci@cataniastudio.com&subject=Preciso de Ajuda! (Painel - Boutique Werner)&body=Descreva seu problema com máximo de detalhes possível!'>programacao@cataniastudio.com</a>");
    }
    $smarty->assign('message', $message);
endif;

switch($action){
    case 'adicionar': $displayTpl = 'cores/adicionar'; break;
    case 'editar':
        $displayTpl = 'cores/adicionar';

        $colorQuery = $database -> createQueryBuilder();
        $colorQuery -> select('id, name, slug, hex, active')
            -> from('colors')
            -> where('id=:idColor')
            -> setParameter(':idColor',$params[1],\PDO::PARAM_INT);
        $colorStatement = $colorQuery->execute();

        $color = $colorStatement->fetch(\PDO::FETCH_ASSOC);
        $smarty->assign('color', $color);
        break;
    case 'remover':
        $removeColor = $database->delete('colors',['id'=>$params[1],'slug'=>$params[2]],['id'=>\PDO::PARAM_INT,'slug'=>\PDO::PARAM_STR]);

        if($removeColor) $message =[
            'type'=>'success',
            'dismissible'=>true,
            'text'=>"Dados removidos com sucesso!"];
        else $message = [
            'type'=>'danger',
            'dismissible'=>true,
            'text'=>"<strong>Oops! Parece que houve um erro...</strong> Por Favor, tente novamente remover os dados. Caso este erro persista, entre em contato com o nosso suporte: <a href='mailto:programacao@cataniastudio.com?cc=laci@cataniastudio.com&subject=Preciso de Ajuda! (Painel - Boutique Werner)&body=Descreva seu problema com máximo de detalhes possível!'>programacao@cataniastudio.com</a>"];

        $smarty->assign('message',$message);

        $colorsQuery = $database -> createQueryBuilder();
        $colorsQuery -> select('id, name, slug, hex, active') -> from('colors');
        $colorsStatement = $colorsQuery->execute();

        $colors = $colorsStatement->fetchAll(\PDO::FETCH_ASSOC);
        $smarty->assign('colors', $colors);
        break;

    case 'produto':

        $idProduct = $params[1];

        if($params[2] == 'associar'):

            $displayTpl = 'cores/associar';

            $colorsQuery = $database -> createQueryBuilder();
            $colorsQuery -> select('id, name, hex') -> from('colors') -> where('active=:isActive') -> setParameter(':isActive','Y');
            $colorsStatement = $colorsQuery->execute();

            $colors = $colorsStatement->fetchAll(\PDO::FETCH_ASSOC);
            $smarty->assign('colors', $colors);


            $colorsByProductQuery = $database -> createQueryBuilder();
            $colorsByProductQuery -> select('id_color')
                -> from('product_colors')
                -> where('id_product = :idProduct')
                -> setParameter(':idProduct',$idProduct,\PDO::PARAM_INT);
            $colorsByProductStatement = $colorsByProductQuery->execute();

            $product_colors = array();
            while($row = $colorsByProductStatement->fetch(PDO::FETCH_ASSOC)) $product_colors[] = $row['id_color'];
            $smarty->assign('product_colors',$product_colors);

        else:

            $displayTpl = 'cores/produto';

            $getProductNameQuery  = $database -> createQueryBuilder();
            $getProductNameQuery -> select('name') -> from('products') -> where('id=:idProduct') ->setParameter(':idProduct',$idProduct,\PDO::PARAM_INT);
            $getProductNameStatement = $getProductNameQuery -> execute();
            $getProductName = $getProductNameStatement->fetch(\PDO::FETCH_ASSOC);

            $colorsByProductQuery = $database -> createQueryBuilder();
            $colorsByProductQuery -> select('c.id, c.name, c.slug, c.hex, c.active, pc.id as id_product_color')
                -> from('colors','c')
                -> innerJoin('c','product_colors','pc','pc.id_color = c.id')
                -> where('pc.id_product = :idProduct')
                -> setParameter(':idProduct',$idProduct,\PDO::PARAM_INT);
            $colorsByProductStatement = $colorsByProductQuery->execute();

            $smarty->assign('productName',$getProductName['name']);
            $smarty->assign('colorsByProduct',$colorsByProductStatement->fetchAll(\PDO::FETCH_ASSOC));

        endif;
        break; # /.produto
    default:
        $colorsQuery = $database -> createQueryBuilder();
        $colorsQuery -> select('id, name, slug, hex, active') -> from('colors');
        $colorsStatement = $colorsQuery->execute();

        $colors = $colorsStatement->fetchAll(\PDO::FETCH_ASSOC);
        $smarty->assign('colors', $colors);
        break;
}