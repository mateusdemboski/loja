<?php

if(isPost()):

    if($_POST['action'] == 'addOrEditUser'){

        if ( !empty( $_FILES['photo']['name'] ) ) {

            $name = explode( '.' , $_FILES['photo']['name'] );
            $tempPath = $_FILES['photo']['tmp_name'];
            $extension = array_pop( $name );
            $name = implode( '.' , $name );

            $length = rand(0,32);
            $start  = rand(0,$length);

            $fileName = clean($name) . '-' . substr( MD5( uniqid( rand(), true ) ) , $start, $length ) . '.' . $extension;

            $uploadPath = PUBLIC_DIR . DS . 'img'. DS . 'avatars' . DS . $fileName;

            if(move_uploaded_file( $tempPath, $uploadPath )){
                //exclui o arquivo anterior, caso ele existir
                if(file_exists(PUBLIC_DIR.DS.'img'.DS.'avatars'.DS.$_POST['photoOriginal']) &&
                    !is_dir(PUBLIC_DIR.DS.'img'.DS.'avatars'.DS.$_POST['photoOriginal'])
                ) unlink(PUBLIC_DIR.DS.'img'.DS.'avatars'.DS.$_POST['photoOriginal']);

                //Base miníma para imagem
                $base = array( 'width' => 150 , 'height' => 150 );

                //lê a imagem com o Imagine usando a biblioteca GD caso o Imagick não esteja instalado
                $imagine = extension_loaded('imagick') ? new Imagine\Imagick\Imagine() : new Imagine\Gd\Imagine();
                $uploadedImage = $imagine->open($uploadPath);

                //Pega o tamanho da imagem
                $imageSize = $uploadedImage->getSize();

                $_imgx = $imageSize->getWidth();
                $_imgy = $imageSize->getHeight();

                //Atualiza os valores da base para os calculos.
                if($_imgx > $base['width'] || $_imgy > $base['height']){
                    if($_imgx > $base['width']){
                        $base['height'] = round( ( $_imgx * $base['height'] ) / $base['width'] );
                        $base['width']  = $_imgx;
                    }else{
                        $base['width']   = round( ( $_imgy * $base['width'] ) / $base['height'] );
                        $base['height']  = $_imgy;
                    }
                }// endif

                //se algum dos lados da base for menor que a imagem, a imagem original é cortada
                //para adaptarse ao valor da base.
                if($_imgx > $base['width'] || $_imgy > $base['height'])
                    $uploadedImage->crop(new Imagine\Image\Point(0, 0),new Imagine\Image\Box($base['width'],$base['height']));

                $position = calc_position($base, $_imgx, $_imgy);

                //Cria a base para imagem
                $collage = $imagine->create(new Imagine\Image\Box($base['width'],$base['height']));
                //cola a imagem original na base, centralizando-a
                $collage->paste($uploadedImage, new Imagine\Image\Point($position['x'], $position['y']));
                //sobrescreve a imagem original com a nova
                $collage->save($uploadPath);
            }// endif
        } else $fileName = $_POST['photoOriginal'];

        $id       = (int) $_POST['id'];
        $role     = (int) $_POST['role'];
        $passwd   = empty( $_POST['password'] ) ? rand_passwd() : $_POST['password'];
        $password = password_hash($passwd,PASSWORD_BCRYPT, $configurations['password']);

        $userData = [
            'username' => $_POST['username'],
            'name'     => $_POST['name'],
            'image'    => $fileName,
            'email'    => $_POST['email'],
            'password' => $password,
            'id_role'  => $role,
            'active'   => $_POST['active']
        ];
        if( empty( $_POST['password'] ) && !empty( $id ) ) unset( $userData['password'] );

        if(empty($id)){

            $createUser = $database->insert('users', $userData);
            $idUser = $database->lastInsertID();

            if ($createUser) $message = [
                'type' => 'success',
                'text' => 'Usuário adicionado com sucesso ao sistema.'];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];
        }else{

            $updateUser = $database->update('users', $userData,['id'=>$id]);

            if ($updateUser) $message = [
                'type' => 'success',
                'text' => 'Dados do usuário atualizado com sucesso.'];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

        }// endif
    }elseif($_POST['action'] == 'updatePermissions'){
        $checking    = array();
        $permissions = (array) $_POST['permissions'];
        if($database->delete('users_permissions',['id_user'=>(int) $_POST['id']]))
            foreach($permissions as $permission)
                $checking[] = $database->insert(
                    'users_permissions',
                    [
                        'id_user'=>(int) $_POST['id'],
                        'id_permission'=>(int) $permission
                    ]
                );

        if(in_array(0,$checking))$message = [
            'type'=>'danger',
            'text'=>'<strong>Oops, Parece que houve um erro...</strong> Parece que ao menos uma das permições selecionadas não foi gravada corretamente. Por favor, tente novamente. Caso este erro persista, entre em contato conosco!'
        ];

        else $message = [
            'type'=>'success',
            'text'=>'<strong>Permições atualizadas com sucesso!</strong>'
        ];


    }else $message = [
        'type'=>'danger',
        'text'=>'<strong>Oops, Parece que houve um erro</strong>: A ação solicitada é inválida!'
    ]; // endif

    $smarty->assign('message', $message);
endif;

switch($action){
    case 'adicionar':

        $rolesQuery = $database -> createQueryBuilder();
        $rolesQuery -> select('id, name') -> from('roles');
        $rolesStatement = $rolesQuery->execute();

        $roles = $rolesStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('roles', $roles);

        $displayTpl = 'usuarios/adicionar';
        break;

    case 'editar':
        $displayTpl = 'usuarios/adicionar';

        $userQuery = $database -> createQueryBuilder();
        $userQuery -> select('username, name, image, email, id_role, active')
                   -> from('users')
                   -> where('id=:idUser')
                   ->setParameter(':idUser',$params[1],\PDO::PARAM_INT);
        $userStatement = $userQuery->execute();

        $user = $userStatement->fetch(\PDO::FETCH_ASSOC);

        $smarty->assign('username',$user['username']);
        $smarty->assign('name',$user['name']);
        $smarty->assign('image',$user['image']);
        $smarty->assign('email',$user['email']);
        $smarty->assign('id_role',$user['id_role']);
        $smarty->assign('active',$user['active']);

        $rolesQuery = $database -> createQueryBuilder();
        $rolesQuery -> select('id, name') -> from('roles');
        $rolesStatement = $rolesQuery->execute();

        $roles = $rolesStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('roles', $roles);

        break;

    case 'remover':
        $idUser        = (int) $params[1];

        $userDataQuery = $database -> createQueryBuilder();
        $userDataQuery -> select('id, image') -> from('users') -> where('id=:idUser') ->setParameter(':idUser',$idUser,\PDO::PARAM_INT);
        $userDataStatement = $userDataQuery->execute();
        $userData = $userDataStatement->fetch(\PDO::FETCH_ASSOC);
        $userPhotoPath = PUBLIC_DIR . DS . 'img' . DS . 'avatars' . DS . $userData['image'];

        if($userData['id'] == $idUser){
            $excludedUser = $database->delete('users',['id'=> $userData['id']]);
            if(file_exists($userPhotoPath) && !is_dir($userPhotoPath))
                unlink($userPhotoPath);

            if ($excludedUser) $message = [
                'type' => 'success',
                'text' => "Usuário removido com sucesso."];

            else $message = [
                'type' => 'danger',
                'text' => '<strong>Oops, Parece que houve um erro...</strong> Por favor, tente novamente! Caso este erro persista entre em contato conosco.'];

        }else $message = [
            'type' => 'danger',
            'text' => '<strong>Oops, Parece que houve um erro...</strong> O item selecionado é inválido!'];

        $usersQuery = $database -> createQueryBuilder();
        $usersQuery -> select('id, name, image, active') -> from('users') -> where('id<>1');
        $usersStatement = $usersQuery->execute();
        $users = $usersStatement->fetch(\PDO::FETCH_ASSOC);

        $smarty->assign('users',$users);
        break;

    default:
        $usersQuery = $database -> createQueryBuilder();
        $usersQuery -> select('id, name, image, active') -> from('users') -> where('id<>1');
        $usersStatement = $usersQuery->execute();
        $users = $usersStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('users', $users);
        break;
}