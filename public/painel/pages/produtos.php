<?php

if(isPost()):

    if($_POST['action'] == 'addOrEditProduct'):

        if ( !empty( $_FILES['photo']['name'] ) ) {

            $name = explode( '.' , $_FILES['photo']['name'] );
            $tempPath = $_FILES['photo']['tmp_name'];
            $extension = array_pop( $name );
            $name = implode( '.' , $name );

            $length = rand(0,32);
            $start  = rand(0,$length);

            $fileName = clean($name) . '-' . substr( MD5( uniqid( rand(), true ) ) , $start, $length ) . '.' . $extension;

            $uploadPath = $configurations['UPLOAD_DIRECTORY'] . DS . 'products' . DS . $fileName;

            if(move_uploaded_file( $tempPath, $uploadPath )){
                //exclui o arquivo anterior, caso ele existir
                if(file_exists($configurations['UPLOAD_DIRECTORY'].DS.'products'.DS.$_POST['photoOriginal']) &&
                    !is_dir($configurations['UPLOAD_DIRECTORY'].DS.'products'.DS.$_POST['photoOriginal'])
                ) unlink($configurations['UPLOAD_DIRECTORY'].DS.'products'.DS.$_POST['photoOriginal']);

                //Base miníma para imagem
                $base = array( 'width' => 560 , 'height' => 840 );

                //lê a imagem com o Imagine usando a biblioteca GD caso o Imagick não esteja instalado
                $imagine = extension_loaded('imagick') ? new Imagine\Imagick\Imagine() : new Imagine\Gd\Imagine();
                $uploadedImage = $imagine->open($uploadPath);

                //Pega o tamanho da imagem
                $imageSize = $uploadedImage->getSize();

                $_imgx = $imageSize->getWidth();
                $_imgy = $imageSize->getHeight();

                //Atualiza os valores da base para os calculos.
                if($_imgx > $base['width'] || $_imgy > $base['height']){
                    if($_imgx > $base['width']){
                        $base['height'] = round( ( $_imgx * $base['height'] ) / $base['width'] );
                        $base['width']  = $_imgx;
                    }else{
                        $base['width']   = round( ( $_imgy * $base['width'] ) / $base['height'] );
                        $base['height']  = $_imgy;
                    }
                }// endif

                //se algum dos lados da base for menor que a imagem, a imagem original é cortada
                //para adaptarse ao valor da base.
                if($_imgx > $base['width'] || $_imgy > $base['height'])
                    $uploadedImage->crop(new Imagine\Image\Point(0, 0),new Imagine\Image\Box($base['width'],$base['height']));

                $position = calc_position($base, $_imgx, $_imgy);

                //Cria a base para imagem
                $collage = $imagine->create(new Imagine\Image\Box($base['width'],$base['height']));
                //cola a imagem original na base, centralizando-a
                $collage->paste($uploadedImage, new Imagine\Image\Point($position['x'], $position['y']));
                //sobrescreve a imagem original com a nova
                $collage->save($uploadPath);
            }// endif
        } else $fileName = $_POST['photoOriginal'];

        $id          = (int) $_POST['id'];
        $namedAction = empty($id) ? 'cadastra': 'altera';
        $data        = array(
            'image'                  => $fileName,
            'reference'              => $_POST["reference"],
            'name'                   => $_POST["name"],
            'slug'                   => $_POST["slug"],
            'description'            => $_POST["description"],
            'specification'          => $_POST["specification"],
            'cost_price'             => str_replace('.','', $_POST["cost_price"]),
            'price'                  => str_replace('.','', $_POST["price"]),
            'promotional_price'      => str_replace('.','', $_POST["promotional_price"]),
            'parcel_max'             => $_POST["parcel_max"],
            'stock'                  => $_POST["stock"],
            'weight'                 => str_replace('.','', $_POST["weight"]),
            'id_department'          => $_POST["department"],
            'deadline'               => $_POST["deadline"],
            'free_shipping'          => $_POST["free_shipping"],
            'active'                 => $_POST["active"],
            'featured_in_home'       => $_POST["featured_in_home"],
            'featured_in_department' => $_POST["featured_in_department"],
            'created_by_id_user'     => $_SESSION['user']['id'],
            'created_on'             => date('Y-m-d H:i:s')
        );
        $make = empty($id) ? $database->insert('products', $data) : $database->update( 'products', $data, array('id' => $id));

        if($make) $message = array('type'=>'success', 'dismissible'=>true, 'text'=>"Dados {$namedAction}dos com sucesso!");
        else $message = array('type'=>'danger', 'dismissible'=>true, 'text'=>"<strong>Oops! Parece que houve um erro...</strong> Por Favor, tente novamente {$namedAction}r os dados. Caso este erro persista, entre em contato com o nosso suporte: <a href='mailto:programacao@cataniastudio.com?cc=laci@cataniastudio.com&subject=Preciso de Ajuda! (Painel - Boutique Werner)&body=Descreva seu problema com máximo de detalhes possível!'>programacao@cataniastudio.com</a>");
    endif;

    $smarty->assign('message', $message);
endif;

switch($action){
    case 'adicionar':
        $displayTpl = 'produtos/adicionar';
        $queryDepartments = $database->createQueryBuilder();
        $queryDepartments -> select('id, name')
                          -> from('departments')
                          -> where('active=:isActive')
                          -> setParameter(':isActive','Y');;
        $departmentsStatement = $queryDepartments->execute();

        $smarty->assign('departments', $departmentsStatement->fetchAll(\PDO::FETCH_ASSOC));
        break;

    case 'editar':
        $displayTpl = 'produtos/adicionar';
        $queryProduct = $database -> createQueryBuilder();
        $queryProduct -> select('reference, image, name, slug, description, specification, cost_price, price, promotional_price, parcel_max,stock, weight, id_department, deadline, free_shipping, active, featured_in_department, featured_in_home')
                      -> from('products')
                      -> where('id=:idProduct')
                      -> setParameter(':idProduct',$params[1],\PDO::PARAM_INT);
        $productStatement = $queryProduct->execute();
        $product = $productStatement->fetch(\PDO::FETCH_ASSOC);

        $smarty->assign('product',$product);

        $queryDepartments = $database -> createQueryBuilder();
        $queryDepartments -> select('id, name')
            -> from('departments')
            -> where('active=:isActive')
            -> setParameter(':isActive','Y');
        $departmentsStatement = $queryDepartments->execute();
        $departments = $departmentsStatement->fetchAll(\PDO::FETCH_ASSOC);

        $smarty->assign('departments', $departments);
        break;

    default:
        $queryProducts = $database->createQueryBuilder();
        $queryProducts -> select('p.id, p.reference, p.image, p.name, p.slug, p.price, d.name as department, d.slug as department_slug, p.active')
                       -> from('products', 'p')
                       -> innerJoin('p','departments','d', 'd.id = p.id_department');
        $productsStatement = $queryProducts -> execute();
        $smarty->assign('products',$productsStatement->fetchAll(\PDO::FETCH_ASSOC));
        break;
}