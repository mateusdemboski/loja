<?php
require('../core/definitions.php');
require(CORE_DIR . DS . 'config.php');
require(CORE_DIR . DS . 'functions.php');
require(VENDOR_DIR . DS . 'autoload.php');

if(isPost()):

    $loginError = [
        'status'   => false,
        'title'    => 'Não foi possivel fazer login com os dados fornecidos!',
        'message'  => 'Usuário e/ou Senha inválido(s)',
    ];

    //Set the connection to Database
    $dbconfig = new \Doctrine\DBAL\Configuration();
    $database = \Doctrine\DBAL\DriverManager::getConnection($configurations['database'],$dbconfig);

    //try get the user data from received data
    $query = $database->createQueryBuilder();
    $query -> select('id, username, name, image, password, id_role')
           -> from('users')
           -> where('username=:username OR email=:email')
           -> setParameter(':username', $_POST['username'])
           -> setParameter(':email', $_POST['username']);
    $statement = $query->execute();
    $user = $statement->fetch(\PDO::FETCH_ASSOC);

    //checks if the query returned a value
    if(isset($user['id'])){
        //Verifies that the password entered match the that is in the database
        if(password_verify($_POST['password'], $user['password'])){
            //All OK. Starting session
            session_start();

            //get the name of role for this user.
            $queryRoleName = $database->createQueryBuilder();
            $queryRoleName -> select('name')
                           -> from('roles')
                           -> where('id=:idRole')
                           -> setParameter(':idRole',$user['id_role'],PDO::PARAM_INT);
            $roleNameStatement = $queryRoleName->execute();
            $roleName = $roleNameStatement->fetch(\PDO::FETCH_ASSOC);

            //save user data in a session
            $_SESSION['user'] = array(
                'id'          => $user['id'],
                'name'        => $user['name'],
                'image'       => $user['image'],
                'role'        => $roleName['name'],
                'id_role'     => $user['id_role'],
            );

            //save user role permissions in a session
            $queryRolePermissions = $database->createQueryBuilder();
            $queryRolePermissions -> select('id_permission')
                                  -> from('role_permissions')
                                  -> where('id_role=:idUser')
                                  -> setParameter(':idUser',$user['id_role'],PDO::PARAM_INT);
            $rolePermissionsStatement = $queryRolePermissions->execute();

            $_SESSION['role_permissions'] = $rolePermissionsStatement->fetch(\PDO::FETCH_ASSOC);

            //moves the user for home or to the previously requested page.
            header('Location: /');exit();

        } else $loginError['status'] = true; // return a error
    } else $loginError['status'] = true; // return a error

endif;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex,nofollow" />
    <title>Catânia Studio | Login</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/square/red.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page">
<?php if(isset($_GET['logout'])): ?>
    <div class="container">
        <div class="alert alert-info alert-dismissable" style="margin-top:20px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i> Logout!</h4>
            Você fez logout do sistema.
        </div>
    </div>
<?php elseif(isset($_GET['notLogged'])): ?>
    <div class="container">
        <div class="alert alert-warning alert-dismissable" style="margin-top:20px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i> Você não fez login no sistema!</h4>
            Para acessara a página solicitada você primeiro deve fazer login no sistema.
        </div>
    </div>
<?php elseif($loginError['status']): ?>
    <div class="container">
        <div class="alert alert-danger alert-dismissable" style="margin-top:20px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-info"></i> <?=$loginError['title']?>!</h4>
            <?=$loginError['message']?>
        </div>
    </div>
<?php endif; ?>
<div class="login-box">
    <div class="login-logo">
        <a href="<?=$configurations['SITE_URL']?>"><b>Admin</b>LTE</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Faça login para iniciar sua sessão</p>
        <form method="post" action="login.php">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" placeholder="Usuário" value="<?=$_COOKIE['username']?>" required="" autofocus="" />
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Senha" required=""/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <a class="btn btn-default btn-block btn-flat" href="<?=$configurations['SITE_URL']?>">Voltar para o site</a>
                </div><!-- /.col -->
                <div class="col-xs-4 col-xs-offset-2">
                    <button type="submit" class="btn btn-danger btn-block btn-flat">Entrar</button>
                </div><!-- /.col -->
            </div>
        </form>
        <br>
        <div class="row">
            <div class="col-xs-12 text-center">
                <a href="#lostMyPassword" class="btn-link" data-toggle="modal" data-target="#lostMyPassword">Esqueci minha senha</a>
            </div>
        </div>

    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<div class="modal fade" id="lostMyPassword" tabindex="-1" role="dialog" aria-labelledby="lostMyPassword" aria-hidden="true">
    <div class="modal-dialog">
        <form method="post" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Esqueci Minha Senha</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="sr-only" for="username">Usuário</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Usuário" required="">
                    </div>
                    <br>
                    <label class="sr-only" for="email">E-mail</label>
                    <div class="input-group">
                        <div class="input-group-addon"><strong>@</strong></div>
                        <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" required="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Recuperar senha</button>
            </div>
        </form><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- jQuery 2.1.3 -->
<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-red',
            radioClass: 'iradio_square-red',
            increaseArea: '20%' //optional
        });
    });
</script>
</body>
</html>