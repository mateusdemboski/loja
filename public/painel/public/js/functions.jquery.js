$(function(){

    $('#excludeMessage').click(function(){
        var messageID = parseInt($("input[name='id_contact']:checked").val());
        var action    = (box != 'Trash') ? 'remover' : 'lixo';
        if(messageID){
            if(confirm('Deseja realmente excluir está mensagem?'))
                window.location = path + '/contatos/'+ action +'/' + messageID;
        }else
            toastr.warning('A mensagem solicitada é inválida!');

        return false;

    });

    $('#replyMessage').click(function(){
        var messageID = parseInt($("input[name='id_contact']:checked").val());
        if(messageID)
            window.location = path + '/contatos/responder/' + messageID;
        else
            toastr.warning('A mensagem solicitada é inválida!');

        return false;

    });

    $('#forwardMessage').click(function(){
        var messageID = parseInt($("input[name='id_contact']:checked").val());
        if(messageID)
            window.location = path + '/contatos/encaminhar/' + messageID;
        else
            toastr.warning('A mensagem solicitada é inválida!');

        return false;

    });

});
