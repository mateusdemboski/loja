$(function () {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"], input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_flat-red',
        radioClass: 'iradio_flat-red'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
            //Uncheck all checkboxes
            $("input[type='checkbox']").iCheck("uncheck");
        } else {
            //Check all checkboxes
            $("input[type='checkbox']").iCheck("check");
        }
        $(this).data("clicks", !clicks);
    });

    $('#wysiwyg-ckeditor').ckeditor();
    $('#compose-textarea').ckeditor();
    $('.ckeditor').ckeditor();

    $(".color-picker").colorpicker();

    $('.mask-money').maskMoney({
        prefix:'R$ ',
        allowNegative: false,
        thousands:'.',
        decimal:',',
        affixesStay: false
    });

    $('.weight').maskMoney({
        prefix:'',
        affixesStay: false,
        allowNegative: false,
        thousands:'',
        decimal:',',
        precision: 3,
        allowZero: true
    });

    $("#dataTable").dataTable({
        oLanguage: {
            sProcessing:     "Processando...",
            sSearch:         "Buscar&nbsp;:",
            sLengthMenu:     "Mostrando _MENU_ registros por página",
            sInfo:           "Mostrando do _START_ ao _END_ de _TOTAL_ registros",
            sInfoEmpty:      "Mostrando de 0 até 0 de 0 registros",
            sZeroRecords:    "Nenhum dado foi encontado",
            sInfoFiltered:   "(filtrado de _MAX_ registros totais)",
            sLoadingRecords: "Carregando registros...",
            oPaginate: {
                sFirst:      "Primeiro",
                sPrevious:   "Anterior",
                sNext:       "Seguinte",
                sLast:       "Último"
            }
        }
    });

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
});

$(function(){
    $('#excludeMessage').click(function(){

    });

    //Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function (e) {
        e.preventDefault();
        //detect type
        var $this = $(this).find("a > i");
        var glyph = $this.hasClass("glyphicon");
        var fa = $this.hasClass("fa");

        //Switch states
        if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
        }

        if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
        }
    });
});