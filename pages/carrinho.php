<?php
//creates a session key to shopping cart
if(!isset($_SESSION['shoppingCart'])) $_SESSION['shoppingCart'] = array();
//creates a session key to store temporarily customer data
if(!isset($_SESSION['customer'])) $_SESSION['customer'] = array();

$action = $params[0];

if(isPost()){
    //default response to not allowed posts to this page
    $response  = array('success'=>false,'message'=>'A ação solicitada falhou!');

    $productID = (int) $_POST['id'];
    $quantity  = (int) $_POST['quantity'];
    $size      = (int) $_POST['size'];
    $color     = (int) $_POST['color'];
    $gift      = (bool) $_POST['gift'];

    //creates a reference to item in cart by identification, color and size
    $reference = empty($_POST['reference']) ? $productID.$size.$color : $_POST['reference'];

    if($action == 'add' && $productID != 0){

        if(!isset($_SESSION['shoppingCart'][$reference])){
            $_SESSION['shoppingCart'][$reference] = base64_encode(serialize(array('id'=>$productID, 'quantity'=>$quantity, 'size'=>$size, 'color'=>$color, 'gift'=>$gift)));

            $response['success'] = true;
            $response['message'] = 'O produto foi adicionado ao carrinho!';
        }else{
            $productInCart = unserialize(base64_decode($_SESSION['shoppingCart'][$reference]));
            $productInCart['quantity'] += $quantity;
            $_SESSION['shoppingCart'][$reference] = base64_encode(serialize($productInCart));

            $response['message'] = 'Quantidade do produto atualizada no carrinho.';
        }
        #/END OF ACTION 'add'
    }elseif($action == 'remove' && $reference != 011 ){

        if(isset($_SESSION['shoppingCart'][$reference])){
            unset($_SESSION['shoppingCart'][$reference]);
            $response['success'] = true;
            $response['message'] = 'Produto removido do carrinho!';
        }
        else $response['message'] = 'O produto solicitado para remoção não está em seu carrinho!';

        #/END OF ACTION 'remove'
    }elseif($action == 'update' && $reference != 011){

        if(isset($_SESSION['shoppingCart'][$reference])){

            $response['success'] = true;
            $response['message'] = 'O produto atualizado com sucesso!';
        }
        else $response['message'] = 'O produto solicitado para atualização não está em seu carrinho!';

        #/END OF ACTION 'update'
    }

    //para a exucução do código e exibe a resposta para solicitação feita
    exit(json_encode($response,JSON_NUMERIC_CHECK));
}else{

    $smarty->assign('pageTitle','Carrinho de compras');
    $smarty->assign('metaRobots','noindex,nofollow');

    $shoppingCart = $_SESSION['shoppingCart'];
    $products     = array();

    foreach($shoppingCart as $reference => $product)
        $products[$reference] = $database->read('products',['reference', 'name', 'slug', 'price', 'promotional_price', 'stock'],'active="Y" AND id=:productID',['productID'=>$productID],'id',1);

    $smarty->assign('products', $products);
}