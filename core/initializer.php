<?php
/**
 * @author Mateus Demboski <mateus@hospedasul.com>
 * @since 04/04/14
 * @global $url;
 */
require(CORE_DIR.DS.'config.php');
require(CORE_DIR.DS.'functions.php');
if(file_exists(VENDOR_DIR.DS.'autoload.php')) require(VENDOR_DIR.DS.'autoload.php');
else die('You have already installed the dependencies with composer?');

//Set the connection to Database
$database = new CRUD(new PDO(
                             $configurations['database']['driver'] . ':host=' . $configurations['database']['host'] . ';port=' . $configurations['database']['host'] . ';dbname=' . $configurations['database']['name'] . ';charset=' . $configurations['database']['charset'],
                             $configurations['database']['user'],
                             $configurations['database']['password']));

//Set configurations for Smarty Template Engine
$smarty = new Smarty();
$smarty->template_dir   = DIRECTORY_ROOT.DS.'views'.DS;
$smarty->compile_dir    = DIRECTORY_ROOT.DS.'tmp'.DS.'templates_c'.DS;
$smarty->config_dir     = CORE_DIR.DS.'smarty_configs'.DS;
$smarty->cache_dir      = DIRECTORY_ROOT.DS.'tmp'.DS.'cache'.DS;

#register my custom smarty functions
$smarty->registerPlugin('function','css','smarty_function_css');
$smarty->registerPlugin('function','script','smarty_function_script');

//Set routing and párams
$route = new Routes('pages', $url);
$route->run();

$smarty->assign('path', $route->getPathForURL());

$pageFile = DIRECTORY_ROOT.DS.$route->getPathForInclude();

$params = (array) $route->getParamsFromURL();

//verify and include the requested page
if(file_exists($pageFile)){

    include($pageFile);
    $smarty->assign('route', $route->getFileName());
    if(isset($displayTpl)){$smarty->assign('mid','../views/' . $displayTpl . '.tpl');
    }else{$smarty->assign('mid','../views/' . $route->getFileName() . '.tpl');}
    $smarty->assign('params', $params);
}else{
    header("HTTP/1.0 404 Not Found");
    $smarty->assign('mid', '../views/default/404.tpl');
    $smarty->assign('pageTitle','Erro!');
    $smarty->assign('mensage', ['type'=>'danger', 'dismissible'=>true, 'value'=>"ROUTE '{$route->getFileName()}' NOT FOUND! (error 404)"]);
}

$smarty->display('layout.tpl');
