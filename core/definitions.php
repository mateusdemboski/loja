<?php
/**
 * @author Mateus Demboski <mateus@hospedasul.com>
 * @since 04/04/14
 */
$siteFolder = '';
$url = trim(parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH), '/\\');
$_explodeUrl = explode('/',$url);
$_explodeDirectory = explode(DS,__DIR__);
if(in_array($_explodeUrl[0], $_explodeDirectory) && isset($_explodeUrl[0]) && !empty($_explodeUrl[0])){
    $siteFolder .= DS.array_shift($_explodeUrl);
    while(isset($_explodeUrl[0]) && in_array($_explodeUrl[0], $_explodeDirectory) && !empty($_explodeUrl[0])) $siteFolder .= DS.array_shift($_explodeUrl);
}

if(!defined(DIRECTORY_ROOT)) define('DIRECTORY_ROOT',DS.trim($_SERVER['DOCUMENT_ROOT'],DS).$siteFolder);
if(!defined('CORE_DIR')) define('CORE_DIR', DIRECTORY_ROOT.DS.'core');
if(!defined('PAGES_DIR')) define('PAGES_DIR', DIRECTORY_ROOT.DS.'pages');
if(!defined('VENDOR_DIR')) define('VENDOR_DIR', DIRECTORY_ROOT.DS.'vendor');
