<div class="container">
    <div class="upper-foot">
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <h2>Contact detail</h2>
                <address>
                    <i class="fa fa-map-marker"></i> Rua João Petry, 159 - Industrial<br>
                    Três Coroas - RS | CEP: 95660-000<br>
                    <i class="fa fa-phone"></i> Telefone. (51) 3546-1234<br>
                    <i class="fa fa-fax"></i> Fax. (123) 456-7890<br>
                    <i class="fa fa-envelope"></i> E-mail. <a href="mailto:tamara@wernercalcados.com.br">tamara@wernercalcados.com.br</a>
                </address>
            </div>
            <div class="col-xs-6 col-sm-3">
                <h2>Useful links</h2>
                <ul class="list-unstyled">
                    <li><a href="#">Track Orders</a></li>
                    <li><a href="#">FAQs</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Free Credit Report</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Community Guidelines</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-3">
                <h2>Tags</h2>
                <ul class="list-inline tagclouds">
                    <li><a href="#">Image</a></li>
                    <li><a href="#">Features</a></li>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">Post Formats</a></li>
                    <li><a href="#">Typography</a></li>
                    <li><a href="#">WooCommerce</a></li>
                    <li><a href="#">Shortcodes</a></li>
                    <li><a href="#">Best Sellers</a></li>
                    <li><a href="#">Slideshow</a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-3">
                <h2>Não perca as novidades</h2>
                <p>In venenatis neque a eros laoreet eu placerat erat suscipit. Fusce cursus, erat ut scelerisque.</p>
                <form class="form-inline form-newsletter" role="form">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputEmail2">Endereço de e-mail</label>
                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Digite seu e-mail aqui">
                    </div>
                    <button type="submit" class="btn"><i class="fa fa-caret-right"></i></button>
                </form>
            </div>
        </div>
    </div>
    <div class="below-foot">
        <div class="row">
            <div class="col-xs-6 copyrights">
                <p>Copyright © 2015 Werner Calçados Online. <a target="_blank" href="http://wernercalcados.com/">Werner Calçados</a><br>
                    Todos os direitos reservados.</p>
            </div>
            <div class="col-xs-6 text-right">
                <ul class="list-inline social-list">
                    <li><a data-toggle="tooltip"
                           data-placement="top"
                           title="Facebook"
                           href="https://www.facebook.com/BoutiqueWerner"><i class="fa fa-facebook"></i></a></li>
                    <li><a data-toggle="tooltip"
                           data-placement="top"
                           title="Twitter"
                           href="https://twitter.com/BoutiqueWerner"><i class="fa fa-twitter"></i></a></li>
                    <li><a data-toggle="tooltip"
                           data-placement="top"
                           title="Pinterest"
                           href="http://www.pinterest.com/wernercalcados/"><i class="fa fa-pinterest"></i></a></li>
                    <li><a data-toggle="tooltip"
                           data-placement="top"
                           title="Instagram"
                           href="http://instagram.com/WernerCalcados"><i class="fa fa-instagram"></i></a></li>
                    <li><a data-toggle="tooltip"
                           data-placement="top"
                           title="YouTube"
                           href="https://www.youtube.com/user/wernercalcados"><i class="fa fa-youtube"></i></a></li>
                    <li><a data-toggle="tooltip"
                           data-placement="top"
                           title="Google Plus"
                           href="https://plus.google.com/100002046388155351519/posts"><i class="fa fa-google-plus"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>