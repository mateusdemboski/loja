<form id="form-login" role="form">
    <h4>Login</h4>
    <p>Se você já é cadastrado, faça seu login.</p>
    <div class="form-group">
        <label for="inputusername">Usuário</label>
        <input type="text" class="form-control input-lg" id="inputusername" placeholder="Usuário">
    </div>
    <div class="form-group">
        <label for="inputpassword">Senha</label>
        <input type="password" class="form-control input-lg" id="inputpassword" placeholder="Senha">
    </div>
    <ul class="list-inline">
        <li><a href="#">Criar uma nova conta</a></li>
        <li><a href="#">Esqueci minha senha</a></li>
    </ul>
    <button type="submit" class="btn btn-white">Log in</button>
</form>