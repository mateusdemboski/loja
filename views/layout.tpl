<!DOCTYPE html>
<html>
<head>
    {include file="../views/default/metatags.tpl"}
    <title>Boutique Werner Online - {$pageTitle} | Sapatos, Scarpins, Botas, Calçados, Sandálias, rasteiras, sapatilhas</title>
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap 3 -->
    <link href="{$path}/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Icon Fonts -->
    <link href="{$path}/css/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Owl Carousel Assets -->
    <link href="{$path}/vendor/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="{$path}/vendor/owl-carousel/owl.theme.css" rel="stylesheet">
    <link href="{$path}/vendor/owl-carousel/owl.transitions.css" rel="stylesheet">
    <!-- bxslider -->
    <link href="{$path}/vendor/bxslider/jquery.bxslider.css" rel="stylesheet">
    <!-- flexslider -->
    <link href="{$path}/vendor/flexslider/flexslider.css" rel="stylesheet">
    <!-- Theme -->
    {css files="theme-animate,theme-elements, theme-blog, theme-shop, theme"}
    <!-- Template color yellow -->
    {css file="colors/yellow/style"}
    <!-- Theme Responsive-->
    {css file="theme-responsive"}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div id="page">
        <header>
            <div id="top">
                {include file="../views/partials/top-header.tpl"}
            </div>
            <nav class="navbar navbar-default navbar-main navbar-main-slide" role="navigation">
                {include file="../views/partials/header.tpl"}
            </nav>
        </header>

        <!-- Begin Login -->
        <div class="login-wrapper">
            {include file="../views/partials/login.tpl"}
        </div>
        <!-- End Login -->

        <!-- Begin Main -->
        <div role="main" class="main">
            {include file=$mid}
        </div>
        <!-- End Main -->

        <!-- Begin footer -->
        <footer class="footer">
            {include file="../views/partials/footer.tpl"}
        </footer>
        <!-- End footer -->

    </div>

    <!-- Begin Quickview -->
    <div class="modal fade quickview-wrapper" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <div class="product-detail">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="product-preview">
                                <ul class="bxslider" id="slider1">
                                    <li><img alt="" class="img-responsive" src="{$path}/images/content/products/product-1.jpg"></li>
                                    <li><img alt="" class="img-responsive" src="{$path}/images/content/products/product-1-1.jpg"></li>
                                    <li><img alt="" class="img-responsive" src="{$path}/images/content/products/product-1-2.jpg"></li>
                                    <li><img alt="" class="img-responsive" src="{$path}/images/content/products/product-1-3.jpg"></li>
                                    <li><img alt="" class="img-responsive" src="{$path}/images/content/products/product-1-4.jpg"></li>
                                </ul>

                                <ul class="list-inline bx-pager">
                                    <li><a data-slide-index="0" href=""><img alt="" class="img-responsive" src="{$path}/images/content/products/product-thumb.jpg"></a></li>
                                    <li><a data-slide-index="1" href=""><img alt="" class="img-responsive" src="{$path}/images/content/products/product-thumb-1.jpg"></a></li>
                                    <li><a data-slide-index="2" href=""><img alt="" class="img-responsive" src="{$path}/images/content/products/product-thumb-2.jpg"></a></li>
                                    <li><a data-slide-index="3" href=""><img alt="" class="img-responsive" src="{$path}/images/content/products/product-thumb-3.jpg"></a></li>
                                    <li><a data-slide-index="4" href=""><img alt="" class="img-responsive" src="{$path}/images/content/products/product-thumb-4.jpg"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="summary entry-summary">

                                <h3>Denim Trousers with faux leather details</h3>

                                <div class="reviews-counter clearfix">
                                    <div class="rating five-stars pull-left">
                                        <div class="star-rating"></div>
                                        <div class="star-bg"></div>
                                    </div>
                                    <span>3 Reviews</span>
                                </div>

                                <p class="price">
                                    <span class="amount">$29.99</span>
                                </p>

                                <ul class="list-inline list-select clearfix">
                                    <li>
                                        <div class="list-sort">
                                            <select class="formDropdown">
                                                <option>Select Size</option>
                                                <option>XS</option>
                                                <option>S</option>
                                                <option>M</option>
                                                <option>L</option>
                                                <option>XL</option>
                                                <option>XXL</option>
                                            </select>
                                        </div>
                                    </li>
                                    <li class="color"><a href="#" class="color1"></a></li>
                                    <li class="color"><a href="#" class="color2"></a></li>
                                    <li class="color"><a href="#" class="color3"></a></li>
                                    <li class="color"><a href="#" class="color4"></a></li>
                                </ul>

                                <form method="post" class="cart">
                                    <div class="quantity pull-left">
                                        <input type="button" class="minus" value="-">
                                        <input type="text" class="input-text qty" title="Qty" value="1" name="quantity" min="1" step="1">
                                        <input type="button" class="plus" value="+">
                                    </div>
                                    <a href="#" class="btn btn-grey">
                                        <span><i class="fa fa-heart"></i></span>
                                    </a>
                                    <button href="#" class="btn btn-primary btn-icon"><i class="fa fa-shopping-cart"></i> Add to cart</button>
                                </form>

                                <ul class="list-unstyled product-meta">
                                    <li>Sku: 54329843</li>
                                    <li>Categories: <a href="#">Leather</a> <a href="#">Jeans</a> <a href="#">Men</a></li>
                                    <li>Tags: <a href="#">Shoes</a> <a href="#">Jeans</a> <a href="#">Men</a> <a href="#">T-shirt</a></li>
                                </ul>

                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Description</a> </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <p>Korem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. qui dolorem ipsum quia dolor sit amet.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Addition Information</a> </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body"> <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p> </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Reviews (3)</a> </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body post-comments">
                                                <ul class="comments">
                                                    <li>
                                                        <div class="comment">
                                                            <div class="img-circle"> <img class="avatar" width="50" alt="" src="{$path}/images/content/blog/avatar.png"> </div>
                                                            <div class="comment-block">
                                                                <span class="comment-by"> <strong>Frank Reman</strong></span>
                                                                <span class="date"><small><i class="fa fa-clock-o"></i> January 12, 2013</small></span>
                                                                <p>Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="comment">
                                                            <div class="img-circle"> <img class="avatar" width="50" alt="" src="{$path}/images/content/blog/avatar.png"> </div>
                                                            <div class="comment-block">
                                                                <span class="comment-by"> <strong>Frank Reman</strong></span>
                                                                <span class="date"><small><i class="fa fa-clock-o"></i> July 11, 2014</small></span>
                                                                <p>Nam viverra euismod odio, gravida pellentesque urna varius vitae, gravida pellentesque urna varius vitae</p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="comment">
                                                            <div class="img-circle"> <img class="avatar" width="50" alt="" src="{$path}/images/content/blog/avatar.png"> </div>
                                                            <div class="comment-block">
                                                                <span class="comment-by"> <strong>Frank Reman</strong></span>
                                                                <span class="date"><small><i class="fa fa-clock-o"></i> July 11, 2014</small></span>
                                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Quickview -->

    <!-- Begin Search -->
        {include file="../views/partials/search.tpl"}
    <!-- End Search -->

    <!-- jQuery 1 (necessary for Bootstrap's JavaScript and others plugins) -->
    <script src="{$path}/vendor/jquery.min.js"></script>
    <!-- Bootstrap 3 -->
    <script src="{$path}/bootstrap/js/bootstrap.min.js"></script>
    <script src="{$path}/bootstrap/js/bootstrap-hover-dropdown.min.js"></script>
    <script src="{$path}/vendor/owl-carousel/owl.carousel.min.js"></script>
    <script src="{$path}/vendor/modernizr.custom.js"></script>
    <script src="{$path}/vendor/jquery.stellar.js"></script>
    <script src="{$path}/vendor/imagesloaded.pkgd.min.js"></script>
    <script src="{$path}/vendor/masonry.pkgd.min.js"></script>
    <script src="{$path}/vendor/jquery.pricefilter.js"></script>
    <script src="{$path}/vendor/jquery.bxslider.min.js"></script>
    <script src="{$path}/vendor/mediaelement-and-player.js"></script>
    <script src="{$path}/vendor/waypoints.min.js"></script>
    <script src="{$path}/vendor/flexslider/jquery.flexslider-min.js"></script>
    <!-- Theme Initializer -->
    {script files="theme.plugins,theme"}

</body>
</html>
