<!-- Begin 404 -->
<div class="page-404">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<p class="ico-emotion"><i class="fa fa-frown-o"></i></p>
				<h2>404</h2>
				<p>Sorry but we couldn't find the page you are looking for. Please check to make sure you've typed the URL correctly. You may also want to search for what you are looking for.</p>
				<form class="form-inline form-search form-search2" class="form-inline" role="form">
					<div class="form-group">
						<label class="sr-only" for="textsearch">Enter text search</label>
						<input type="text" class="form-control input-lg" id="textsearch" placeholder="Enter text search">
					</div>
					<button type="submit" class="btn"><i class="fa fa-search"></i></button>
				</form>
				<p><a href="index.html" class="btn btn-primary btn-lg">Return to home</a></p>
			</div>
		</div>
	</div>
</div>
<!-- End 404 -->